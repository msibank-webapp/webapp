<?php
session_start(); //pour pouvoir utiliser les variables de session de l'utilisateur (ex:loggedin booleen pour voir s'il est connecté)

require './config/db.php';
require_once './app/controllers/PlanningController.php';
require_once './app/controllers/AccueilController.php';
require_once './app/controllers/TacheAdministrativeController.php';
require_once './app/controllers/ClientController.php';
require_once './app/controllers/CompteController.php';
require_once './app/controllers/ContratController.php';
require_once './app/controllers/EmployeManagementController.php';
require_once './app/controllers/StatisticController.php';
require_once './app/controllers/MotifController.php';
require_once './app/controllers/JustificatifController.php';
require_once './app/controllers/RendezvousController.php';

// Use parse_url() and rtrim() to clean up the REQUEST_URI and extract the path
$requestPath = rtrim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), '/');

// Redirect user to login page if not logged in
if (!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] !== true) {
    header('Location: /public/login.php'); //header : change l'entête et on l'envoie sur login.php
    exit;
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    switch ($requestPath) {
        case '/tacheadministrative/delete':
            $controller = new TacheAdministrativeController($pdo);
            $controller->delete($_POST);
            break;
        case '/tacheadministrative/create':
            $controller = new TacheAdministrativeController($pdo);
            $controller->create($_POST);
            break;
        case '/tacheadministrative/update':
            $controller = new TacheAdministrativeController($pdo);
            $controller->update($_POST);
            break;
        case '/compte/create':
            $controller = new CompteController($pdo);
            $controller->create($_POST);
            break;
        case '/compte/update':
            $controller = new CompteController($pdo);
            $controller->update($_POST);
            break;
        case '/contrat/create':
            $controller = new ContratController($pdo);
            $controller->create($_POST);
            break;
        case '/operation/create':
            $controller = new CompteController($pdo);
            $controller->newOperation($_POST);
            break;
        case '/employe/create':
            $controller = new EmployeManagementController($pdo);
            $controller->createEmploye($_POST);
            break;
        case '/employe/update':
            $controller = new EmployeManagementController($pdo);
            $controller->EditCredentials($_POST);
            break;
        case '/client/create':
            $controller = new ClientController($pdo);
            $controller->createClient($_POST);
            break;
        case '/client/update':
            $controller = new ClientController($pdo);
            $controller->updateClient($_POST);
            break;
        case '/motif/create':
            $controller = new MotifController($pdo);
            $controller->createMotif($_POST);
            break;
        case '/motif/link-justificatif':
            $controller = new MotifController($pdo);
            $controller->linkJustificatif($_POST);
            break;
        case '/justificatif/create':
            $controller = new JustificatifController($pdo);
            $controller->createJustificatif($_POST);
            break;
        case '/rendezvous/create':
            $controller = new RendezvousController($pdo);
            $controller->create($_POST);
            break;
        case '/statistique/generate':
            $controller = new StatisticController($pdo);
            $controller->generate($_POST);
            break;
    }
    exit;
}

switch ($requestPath) {
    case '/accueil':
        $controller = new AccueilController();
        $controller->index();
        break;
    case '/unauthorized':
        require './public/unauthorized.php';
        break;
    case '/planning':
        $controller = new PlanningController($pdo);
        if (isset($_GET['conseiller_id'])) {
            $controller->showConseillerPlanning($_GET['conseiller_id']);
        } else {
            $controller->showConseillerSelect();
        }
        break;
    case '/client':
        $controller = new ClientController($pdo);
        if (isset($_GET['id'])) {
            $controller->showUnique($_GET['id']);
        } else {
            $controller->showclientList();
        }
        break;
    case '/client/create': //si on veut créer un client
        $controller = new ClientController($pdo);
        $controller->createClientform();
        break;
    case '/client/update': //si on veut modifier un client
        $controller = new ClientController($pdo);
        if (isset($_GET['client_id'])) {
            $controller->updateClientForm($_GET['client_id']);
        } else {
            header('Location: /clients');
        }
        break;
    case '/compte':
        $controller = new CompteController($pdo);
        if (isset($_GET['id'])) {
            $controller->showUnique($_GET['id']);
        }
        break;
    case '/compte/create':
        rolecheck('Conseiller');
        $controller = new CompteController($pdo);
        if (isset($_GET['client_id'])) {
            $controller->showEmpty($_GET['client_id']);
        }
        break;
    case '/compte/update':
        $controller = new CompteController($pdo);
        if (isset($_GET['compte_id'])) {
            $controller->updateForm($_GET['compte_id']);
        }
        break;
    case '/compte/close':
        rolecheck('Conseiller');
        $controller = new CompteController($pdo);
        if (isset($_GET['compte_id'])) {
            $controller->close($_GET['compte_id']);
        }
        break;
    case '/operation/create':
        $controller = new CompteController($pdo);
        if (isset($_GET['compte_id'])) {
            $controller->newEmptyOperation($_GET['compte_id']);
        }
        break;
    case '/contrat':
        $controller = new ContratController($pdo);
        if (isset($_GET['id'])) {
            $controller->showUnique($_GET['id']);
        }
        break;
    case '/contrat/create':
        rolecheck('Conseiller');
        $controller = new ContratController($pdo);
        if (isset($_GET['client_id'])) {
            $controller->showEmpty($_GET['client_id']);
        }
        break;
    case '/contrat/close':
        rolecheck('Conseiller');
        $controller = new ContratController($pdo);
        if (isset($_GET['contrat_id'])) {
            $controller->close($_GET['contrat_id']);
        }
        break;
    case '/employe':
        rolecheck('Directeur');
        $controller = new EmployeManagementController($pdo);
        $controller->EmployeList();
        break;
    case '/employe/create':
        rolecheck('Directeur');
        $controller = new EmployeManagementController($pdo);
        $controller->createEmployeForm();
        break;
    case '/employe/update':
        rolecheck('Directeur');
        $controller = new EmployeManagementController($pdo);
        if (isset($_GET['id'])) {
            $controller->EditEmploye($_GET['id']);
        } else {
            header('Location: /employes');
        }
        break;
    case '/tacheadministrative/create':
        rolecheck('Conseiller');
        $controller = new TacheAdministrativeController($pdo);
        $controller->createForm();
        break;
    case '/tacheadministrative/update':
        rolecheck('Conseiller');
        $controller = new TacheAdministrativeController($pdo);
        if (isset($_GET['id'])) {
            $controller->updateForm($_GET['id']);
        } else {
            header('Location: /planning?conseiller_id=' . $_SESSION['roleId']);
        }
        break;
    case '/rendezvous/create':
        $controller = new RendezvousController($pdo);
        $controller->createForm($_GET['conseiller_id']);
        break;
    case '/rendezvous/detail':
        $controller = new RendezvousController($pdo);
        if (isset($_GET['id'])) {
            $controller->showRendezvousDetail($_GET['id']);
        } else {
            header('Location: /planning');
        }
        break;
    case '/motif':
        rolecheck('Directeur');
        $controller = new MotifController($pdo);
        if (isset($_GET['id'])) {
            $controller->showMotif($_GET['id']);
        } else {
            $controller->showMotifList();
        }
        break;
    case '/motif/create':
        rolecheck('Directeur');
        $controller = new MotifController($pdo);
        $controller->createMotifForm();
        break;
    case '/motif/link-justificatif':
        rolecheck('Directeur');
        $controller = new MotifController($pdo);
        if (isset($_GET['motif_id'])) {
            $controller->linkJustificatifForm($_GET['motif_id']);
        } else {
            header('Location: /motif');
        }
        break;
    case '/justificatif': //si on veut voir les justificatifs
        rolecheck('Directeur');
        $controller = new JustificatifController($pdo);
        $controller->showJustificatifList();
        break;
    case '/justificatif/create': //si on veut créer un justificatif
        rolecheck('Directeur');
        $controller = new JustificatifController($pdo);
        $controller->createJustificatifForm();
        break;
    case '/statistique':
        rolecheck('Directeur');
        $controller = new StatisticController($pdo);
        $controller->showStatisticForm();
        break;
    default:
        header('Location: /accueil');
        exit;
}

function rolecheck($requiredrole)
{
    if ($_SESSION['role'] !== $requiredrole) {
        header('Location: /unauthorized');
        exit;
    }
}

exit;
