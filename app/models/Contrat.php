<?php
//Classe pour la gestion des contrats
Class Contrat {
    protected $pdo;

    // Initialise la classe avec un objet PDO
    public function __construct($pdo) {
        $this->pdo = $pdo;
    }
    // Récupère tous les contrats associès à un ID de client
    public function getContratListByClientID($id) {
        $stmt = $this->pdo->prepare("SELECT Contrat.*, Type_contrat.Nom AS typeContrat FROM Contrat JOIN Type_contrat ON Type_contrat.Id_Type_contrat = Contrat.Id_Type_contrat WHERE Id_Client = ?");
        $stmt->execute([$id]);

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    //Trouve un contrat spéficique par son identifiant 
    public function findById($id) {
        $stmt = $this->pdo->prepare("SELECT Contrat.*, Type_contrat.Nom AS typeContrat, client.Prenom AS prenomClient, client.Nom AS nomClient FROM Contrat 
        JOIN Type_contrat ON Type_contrat.Id_Type_contrat = Contrat.Id_Type_contrat 
        JOIN client ON client.Id_Client = Contrat.Id_Client
        WHERE Id_Contrat = ?");
        $stmt->execute([$id]);

        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    
    //Création d'un nouveau contrat
    public function create($Id_Client, $Type, $DateDebut, $Montant, $libelle) {
        $stmt = $this->pdo->prepare("INSERT INTO Contrat (Id_Client, Id_Type_contrat, Date_ouverture, Tarif_mensuel, Libelle) VALUES (?, ?, ?, ?, ?)");
        $stmt->execute([$Id_Client, $Type, $DateDebut, $Montant, $libelle]);
    }

    //Mise à jour d'un contrat
    public function closeContrat($Id_Contrat, $DateFin) {
        $stmt = $this->pdo->prepare("UPDATE Contrat SET Date_fermeture = ? WHERE Id_Contrat = ?");
        $stmt->execute([$DateFin, $Id_Contrat]);
    }

    /** Types de contrats */

    //Recupère tous les types de contrats existants
    public function getAllContractType() {
        $stmt = $this->pdo->query("SELECT * FROM Contrat");
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    //Création d'un nouveau type de contrat
    public function creatContractType($type) {
        $stmt = $this->pdo->prepare("INSERT INTO Contrat (Type) VALUES (?)");
        $stmt->execute([$type]);
    }

    //Suppression d'un type de contrat par son ID
    public function deleteContractType($id) {
        $stmt = $this->pdo->prepare("DELETE FROM Contrat WHERE Id_TypeContrat = ?");
        $stmt->execute([$id]);
    }

    public function getContratTypes() {
        //requête pour obtenir tous les types de contrats
        $stmt = $this->pdo->query("SELECT * FROM Type_contrat");
        
        return $stmt->fetchAll(PDO::FETCH_ASSOC); //retour des résultats sous forme de tableau
    }

    public function getNbContratBetweenDate($startDate, $endDate) {
        $stmt = $this->pdo->prepare("SELECT COUNT(*) FROM Contrat WHERE Date_ouverture BETWEEN ? AND ?");
        $stmt->execute([$startDate, $endDate]);

        return $stmt->fetchColumn();
    }
}