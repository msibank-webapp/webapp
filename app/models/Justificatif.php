<?php

Class Justificatif { //nouvelle classe
    private $pdo;

    public function __construct($pdo) {
        $this->pdo = $pdo;
    }

    public function create($name) { //nouvelle methode pour la creation de justificatif
        //requête SQL pour insérer une nouvelle ligne dans la table piece_a_fournir avec la colonne Nom.
        $query = $this->pdo->prepare('INSERT INTO piece_a_fournir (Nom) VALUES (?)');
        $query->execute([$name]);
    }

    public function findAll() { // nouvelle methode pour la recherche des jutficatif
        //requête SQL pour sélectionner toutes les lignes de la table piece_a_fournir.
        $query = $this->pdo->query('SELECT * FROM piece_a_fournir');
        return $query->fetchAll();
    }

    public function findById($id) { 
        //requête SQL pour sélectionner une ligne de la table piece_a_fournir en fonction de son Id_Piece_a_fournir.
        $query = $this->pdo->prepare('SELECT * FROM piece_a_fournir WHERE Id_Piece_a_fournir = ?');
        $query->execute([$id]);
        return $query->fetch();
    }
}