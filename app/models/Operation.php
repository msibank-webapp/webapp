<?php
// Définition de la classe Operation pour gérer les opérations bancaires.
Class Operation {
    // Propriété protégée pour stocker une instance de PDO pour les interactions avec la base de données.
    protected $pdo;

    // Constructeur qui initialise l'instance de PDO.
    public function __construct($pdo) {
        // Assignation de l'objet PDO passé en paramètre à la propriété interne pour utilisation ultérieure.
        $this->pdo = $pdo;
    }

    // Méthode pour obtenir toutes les opérations d'un compte spécifique par son ID.
    public function getOperationsByCompteId($id) {
        // Préparation de la requête SQL pour sélectionner toutes les opérations où l'Id_Compte correspond à l'ID fourni.
        // Les opérations sont triées par Date_operation de manière décroissante.
        $stmt = $this->pdo->prepare("SELECT * FROM Operation WHERE Id_Compte = ? ORDER BY Date_operation DESC");
        // Exécution de la requête avec l'ID du compte comme paramètre.
        $stmt->execute([$id]);

         // Récupération de tous les résultats de la requête sous forme de tableau associatif.
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    // Méthode pour créer une nouvelle opération sur un compte.
    public function create($idCompte, $montant, $type, $date) {
         // Préparation de la requête SQL pour insérer une nouvelle opération dans la table Operation.
        // Les valeurs à insérer sont l'ID du compte, le montant de l'opération, le type d'opération, et la date de l'opération.
        $stmt = $this->pdo->prepare("INSERT INTO Operation (Id_Compte, Montant, Type_operation, Date_operation) VALUES (?, ?, ?, ?)");
       // Exécution de la requête avec les valeurs fournies en paramètres.
        $stmt->execute([$idCompte, $montant, $type, $date]);
    }
}