<?php

Class Motif {
    private $pdo;

    public function __construct($pdo) { // Propriété pour stocker l'objet PDO pour les opérations de base de données.

        $this->pdo = $pdo; // Constructeur qui initialise l'objet PDO.
    }

    public function create($motifName, $typeMotif, $libelle) {
        // Gère la création d'un motif basé sur le type de motif.
        switch ($typeMotif) {
            case 'Compte':
                // Prépare une requête SQL pour insérer un motif de type "Compte".
                $typeMotifQuery = $this->pdo->prepare('INSERT INTO type_compte (Nom, Id_Motif) VALUES (?, ?)');
                break;
            case 'Contrat':
                // Prépare une requête SQL pour insérer un motif de type "Contrat".
                $typeMotifQuery = $this->pdo->prepare('INSERT INTO type_contrat (Nom, Id_Motif) VALUES (?, ?)');
                break;
        }
        // Exécute une requête pour insérer le motif général
        $motifQuery = $this->pdo->prepare('INSERT INTO motif (Type_motif, libelle) VALUES (?, ?)');
        $motifQuery->execute([$typeMotif, $libelle]);

         // Récupère l'ID du motif inséré.
        $idMotif = $this->pdo->lastInsertId();

        if ($typeMotif !== 'Autre') {
            // Exécute la requête pour le type spécifique de motif.
            $typeMotifQuery->execute([$motifName, $idMotif]);
        }
        
        return $idMotif;
    }

    public function findAll() {
        // Récupère tous les motifs et leur type.
        $query = $this->pdo->query('SELECT motif.Id_Motif, motif.Type_motif FROM motif');
        $motifsType = $query->fetchAll();

        $motifs = [];
        foreach ($motifsType as $motifType) {
            // Sélectionne les détails du motif basé sur son type.
            switch ($motifType['Type_motif']) {
                case 'Compte':
                    $detailQuery = $this->pdo->prepare('SELECT motif.Id_motif AS Id, type_compte.Nom AS Nom FROM motif INNER JOIN type_compte ON motif.Id_Motif = type_compte.Id_Motif WHERE motif.Id_Motif = ?');
                    $detailQuery->execute([$motifType['Id_Motif']]);
                    break;
                case 'Contrat':
                    $detailQuery = $this->pdo->prepare('SELECT motif.Id_motif AS Id, type_contrat.Nom AS Nom FROM motif INNER JOIN type_contrat ON motif.Id_Motif = type_contrat.Id_Motif WHERE motif.Id_Motif = ?');
                    $detailQuery->execute([$motifType['Id_Motif']]);
                    break;
                case 'Autre':
                    $detailQuery = $this->pdo->prepare('SELECT motif.Id_motif AS Id, motif.Libelle AS Nom FROM motif WHERE Id_Motif = ?');
                    $detailQuery->execute([$motifType['Id_Motif']]);
                    break;
            }
            $motifs[] = $detailQuery->fetch(); // Ajoute les détails récupérés à la liste des motifs.
        }
        return $motifs;
    }

    public function findAllCompteType() {
        $query = $this->pdo->query('SELECT motif.*, type_compte.* FROM motif INNER JOIN type_compte ON motif.Id_Motif = type_compte.Id_Motif');
        return $query->fetchAll();
    }

    public function findAllContratType() {
        $query = $this->pdo->query('SELECT motif.*, type_contrat.* FROM motif INNER JOIN type_contrat ON motif.Id_Motif = type_contrat.Id_Motif');
        return $query->fetchAll();
    }

    public function findAllOtherMotifs() {
        $query = $this->pdo->query('SELECT * FROM motif WHERE motif.Type_motif = "Autre"');
        return $query->fetchAll();
    }

    public function findById($id) {
        $query = $this->pdo->prepare('SELECT motif.Type_motif FROM motif WHERE Id_Motif = ?');
        $query->execute([$id]);
        $typeMotif = $query->fetch()['Type_motif'];

        switch ($typeMotif) {
            case 'Compte':
                $query = $this->pdo->prepare('SELECT motif.Id_motif AS Id, type_compte.Nom AS Nom FROM motif INNER JOIN type_compte ON motif.Id_Motif = type_compte.Id_Motif WHERE motif.Id_Motif = ?');
                break;
            case 'Contrat':
                $query = $this->pdo->prepare('SELECT motif.Id_motif AS Id, type_contrat.Nom AS Nom FROM motif INNER JOIN type_contrat ON motif.Id_Motif = type_contrat.Id_Motif WHERE motif.Id_Motif = ?');
                break;
            case 'Autre':
                $query = $this->pdo->prepare('SELECT motif.Id_motif AS Id, motif.Libelle AS Nom FROM motif WHERE Id_Motif = ?');
                break;
        }
        $query->execute([$id]);
        return $query->fetch();
    }

    public function findJustificatifsByMotifId($id) {
        $query = $this->pdo->prepare('SELECT piece_a_fournir.* FROM necessite JOIN piece_a_fournir ON piece_a_fournir.Id_Piece_a_fournir = necessite.Id_Piece_a_fournir WHERE Id_Motif = ?');
        $query->execute([$id]);
        return $query->fetchAll();
    }

    public function linkJustificatifWithMotif($motifId, $justificatifId) {
        $query = $this->pdo->prepare('INSERT INTO necessite (Id_Motif, Id_Piece_a_fournir) VALUES (?, ?)');
        $query->execute([$motifId, $justificatifId]);
    }

    public function unlinkJustificatifFromMotif($motifId, $justificatifId) {
        $query = $this->pdo->prepare('DELETE FROM necessite WHERE Id_Motif = ? AND Id_Piece_a_fournir = ?');
        $query->execute([$motifId, $justificatifId]);
    }
}