<?php

Class Employe {
    protected $pdo;// propriété protégée $pdo pour la classe Employe

    public function __construct($pdo) { //cette ligne définit un constructeur pour la classe Employe qui accepte un argument
        $this->pdo = $pdo; 
    }

    public function findAll() { //definition d'une nouvelle méthode pour Employe
        $stmt = $this->pdo->prepare("SELECT * FROM Employe"); //SELECTION DE TOUTE LES LIGNES DE LA TABLE EMPLOYE
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC); //renvoie les lignes de la table Employe sous forme de tableau associatif
    }

    public function findById($id) { //definition d'une nouvelle méthode pour Employe
        $stmt = $this->pdo->prepare("SELECT * FROM Employe WHERE Id_Employe = ?"); // sélectionner une ligne de la table Employe en fonction de son Id_Employe
        $stmt->execute([$id]);

        return $stmt->fetch(PDO::FETCH_ASSOC);//renvoie les lignes de la table Employe sous forme de tableau associatif
    }

    public function findConseillerById($id) { //definition d'une nouvelle méthode pour Employe
        //requête SQL pour sélectionner une ligne de la table Employe qui est également un conseiller en fonction de son Id_Conseiller
        $stmt = $this->pdo->prepare("SELECT * FROM Employe INNER JOIN Conseiller ON Conseiller.Id_Employe = Employe.Id_Employe WHERE Conseiller.Id_Conseiller = ?");
        $stmt->execute([$id]);

        return $stmt->fetch(PDO::FETCH_ASSOC);//renvoie les lignes de la table Employe sous forme de tableau associatif
    }

    public function findAgentById($id) { //definition d'une nouvelle méthode pour Employe
       //requête SQL pour sélectionner une ligne de la table Employe qui est également un agent en fonction de son Id_Agent.
        $stmt = $this->pdo->prepare("SELECT * FROM Employe INNER JOIN Agent ON Agent.Id_Employe = Employe.Id_Employe WHERE Agent.Id_Agent = ?");
        $stmt->execute([$id]);

        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    public function editCredentials($id, $login, $password) {//definition d'une nouvelle méthode pour mettre à jour la table Employe 
       // requête SQL pour mettre à jour les colonnes Login et Password de la table Employe en fonction de l'Id_Employe.
        $stmt = $this->pdo->prepare("UPDATE Employe SET Login = ?, Password = ? WHERE Id_Employe = ?");
        $stmt->execute([$login, $password, $id]);
    }

    public function getConseillerList() {
        //requête SQL pour sélectionner les colonnes Prenom, Nom, Id_Employe de la table Employe et Id_Conseiller de la table Conseiller en fonction de la jointure entre les deux tables sur la colonne Id_Employe
        $stmt = $this->pdo->query("SELECT Employe.Prenom, Employe.Nom, Employe.Id_Employe, Conseiller.Id_Conseiller FROM Employe 
        INNER JOIN Conseiller ON Employe.Id_Employe = Conseiller.Id_Employe");
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function create($employeType, $prenom, $nom, $login, $password) { //definition d'une nouvelle méthode pour inseret une ligne dans la table Employe
       //requête SQL pour insérer une nouvelle ligne dans la table Employe avec les colonnes Prenom, Nom, Login, Password et Role.
        $stmt = $this->pdo->prepare("INSERT INTO Employe (Prenom, Nom, Login, Password, Role) VALUES (?, ?, ?, ?, ?)");
       //requête SQL préparée avec les arguments $prenom, $nom, $login, $password et $employeType en tant que valeurs de remplacement pour les marqueurs de position.
        $stmt->execute([$prenom, $nom, $login, $password, $employeType]);

        //récupèration de l'ID de la dernière ligne insérée dans la table Employe.
        $id = $this->pdo->lastInsertId();

        //vérification si le type d'employé est un conseiller.
        if ($employeType === "Conseiller") {
        //requête SQL pour insérer une nouvelle ligne dans la table Conseiller avec la colonne Id_Employe.
            $stmt = $this->pdo->prepare("INSERT INTO Conseiller (Id_Employe) VALUES (?)");
            $stmt->execute([$id]);
        } else {
         //requête SQL préparée avec l'argument $id entant que valeur de remplacement pour le marqueur de position.
            $stmt = $this->pdo->prepare("INSERT INTO Agent (Id_Employe) VALUES (?)");
            $stmt->execute([$id]);
        }
    }
}