<?php
class TacheAdministrative {
    protected $pdo; //propriété protégée pour stocker la connexion à la BDD

    public function __construct($pdo) { // Constructeur : sauvegarde la connexion à la base de données passée lors de la création de l'objet.
        $this->pdo = $pdo;
    }
    // Recupère les tâches d'un conseiller pour une semaine donnée 
    public function getTasksForWeek($conseillerId, $weekStart, $weekEnd) {
        //Prépare et exécute une requête pour sélectionner les tâches dans une plage de dates
        $stmt = $this->pdo->prepare("SELECT Id_Tache_administrative, Date_debut, Date_fin, Libelle FROM Tache_administrative WHERE Id_Conseiller = ? AND Date_debut BETWEEN ? AND ?");
        $stmt->execute([$conseillerId, $weekStart, $weekEnd]);
        return $stmt->fetchAll(PDO::FETCH_ASSOC); //Renvoie les résultats sous forme de tableau.
        //`PDO::FETCH_ASSOC` :le tableau retourné doit utiliser les noms de colonne comme clés. 
        //fetchAll : récupère le jeu de données de la requête executée
    }
    //trouve une tâche par son ID 
    public function findById($id) {
        //Prépare et exécute une requête SQL pour trouver une tâche spécifique par ID 
        $stmt = $this->pdo->prepare("SELECT * FROM Tache_administrative WHERE Id_Tache_administrative = ?");
        $stmt->execute([$id]);
        return $stmt->fetch(PDO::FETCH_ASSOC); 
    }
    // Crée une nouvelle tâche avec les détails fournis 
    public function create($dateDebut, $dateFin, $libelle, $conseillerId) {
        // vérifie que le conseiller n'a pas déjà un rendez-vous à la même date et la même heure
        $stmt = $this->pdo->prepare("SELECT * FROM Rendez_vous WHERE Id_Conseiller = ? AND NOT (Date_fin <= ? OR Date_debut >= ?)");
        $stmt->execute([$conseillerId, $dateDebut, $dateFin]);
        $rdv = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($rdv) {
            throw new Exception("Le conseiller a déjà un rendez-vous ou une tâche administrative à cette date et heure.");
        }

        // même chose pour les tâches administratives 
        $stmt = $this->pdo->prepare("SELECT * FROM Tache_administrative WHERE Id_Conseiller = ? AND NOT (Date_fin <= ? OR Date_debut >= ?)");
        $stmt->execute([$conseillerId, $dateDebut, $dateFin]);
        $tache = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($tache) {
            throw new Exception("Le conseiller a déjà un rendez-vous ou une tâche administrative à cette date et heure.");
        }

       // Prépare et exécute une requête SQL pour insérer une nouvelle tâche
        $stmt = $this->pdo->prepare("INSERT INTO Tache_administrative (Date_debut, Date_fin, Libelle, Id_Conseiller) VALUES (?, ?, ?, ?)");
        return $stmt->execute([$dateDebut, $dateFin, $libelle, $conseillerId]);
    }

    //Met à jour une tâche existante avec de nouvelles informations 
    public function update($id, $dateDebut, $dateFin, $libelle, $conseillerId) {
        // vérifie que le conseiller n'a pas déjà un rendez-vous à la même date et la même heure
        $stmt = $this->pdo->prepare("SELECT * FROM Rendez_vous WHERE Id_Conseiller = ? AND NOT (Date_fin <= ? OR Date_debut >= ?) ");
        $stmt->execute([$conseillerId, $dateDebut, $dateFin]);
        $rdv = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($rdv) {
            throw new Exception("Le conseiller a déjà un rendez-vous ou une tâche administrative à cette date et heure.");
        }

        // même chose pour les tâches administratives 
        $stmt = $this->pdo->prepare("SELECT * FROM Tache_administrative WHERE Id_Conseiller = ? AND NOT (Date_fin <= ? OR Date_debut >= ?) AND Id_Tache_administrative != ?");
        $stmt->execute([$conseillerId, $dateDebut, $dateFin, $id]);
        $tache = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($tache) {
            throw new Exception("Le conseiller a déjà un rendez-vous ou une tâche administrative à cette date et heure.");
        }

        //Prépare et exécute une requête SQL pour mettre à jour les détails d'une tâche
        $stmt = $this->pdo->prepare("UPDATE Tache_administrative SET Date_debut = ?, Date_fin = ?, Libelle = ? WHERE Id_Tache_administrative = ?");
        return $stmt->execute([$dateDebut, $dateFin, $libelle, $id]);
    }
    //Supprime une tâche par son ID
    public function delete($id) {
        //Prépare et exécute une requête SQL pour supprimer une tâche 
        $stmt = $this->pdo->prepare("DELETE FROM Tache_administrative WHERE Id_Tache_administrative = ?");
        return $stmt->execute([$id]);
    }
}
?>