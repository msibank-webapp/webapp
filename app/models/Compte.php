<?php
//classe destinée à gérer des opérations sur des comptes bancaires.
class Compte
{
    //utilisation d'un objet PDO pour intéragir avec la BDD permettant d'effectuer des requêtes 
    protected $pdo; //permet de stocker l'instance de connexion PDO

    //Initialisation de l'instance avec un objet PDO pour les requêtes de BDD
    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    //Récupère une liste de tous les comptes pour un ID client spécifié
    public function getCompteListByClientID($id)
    {
        //Préparation de la requête SQL pour sélectionner les comptes et leur type
        $stmt = $this->pdo->prepare("SELECT Compte.* , Type_compte.Nom AS typeCompte FROM Compte JOIN Type_compte ON Type_compte.Id_Type_compte = Compte.Id_Type_compte WHERE Id_Client = ?");
        $stmt->execute([$id]); //Exécution de la requête

        return $stmt->fetchAll(PDO::FETCH_ASSOC); //retour des résultats sous forme de tableau
    }
    //Trouve un compte spécifique par son identifiant
    public function findById($id)
    {
        //Requete pour obtenir les détails d'un compte par son ID
        $stmt = $this->pdo->prepare("SELECT Compte.* , Type_compte.Nom AS typeCompte , client.Prenom AS prenomClient , client.Nom AS nomClient, client.Id_Client AS Id_Client FROM Compte 
        JOIN Type_compte ON Type_compte.Id_Type_compte = Compte.Id_Type_compte 
        JOIN client ON client.Id_Client = compte.Id_Client
        WHERE Id_Compte = ?");
        $stmt->execute([$id]);

        return $stmt->fetch(PDO::FETCH_ASSOC); //Retour un seul résultat
    }

    // Ouvre un nouveau compte avec des détails spécifiques 
    public function create($Id_Client, $Type, $Solde, $dateOuverture, $decouvert)
    {
        //requête pour inserer un nouveau compte 
        $stmt = $this->pdo->prepare("INSERT INTO Compte (Id_Client, Id_Type_compte, Solde, Date_ouverture, Decouvert) VALUES (?, ?, ?, ?, ?)");
        $stmt->execute([$Id_Client, $Type, $Solde, $dateOuverture, $decouvert]);
    }

    // Ferme un compte en le supprimant de la BDD
    public function closeAccount($id, $dateFermeture)
    {
        $actualSolde = $this->pdo->query("SELECT Solde FROM Compte WHERE Id_Compte = $id")->fetch(PDO::FETCH_ASSOC)['Solde'];
        //requête pour supprimer un compte par son ID 
        $stmt = $this->pdo->prepare("UPDATE Compte SET Date_fermeture = ?, Solde = 0 WHERE Id_Compte = ?");
        $stmt->execute([$dateFermeture, $id]);
        $this->pdo->query("INSERT INTO Operation (Id_Compte, Montant, Type_operation, Date_operation) VALUES ($id, $actualSolde, 'Retrait', '$dateFermeture')");
    }

    public function getCompteTypes()
    {
        //requête pour obtenir tous les types de comptes
        $stmt = $this->pdo->query("SELECT * FROM Type_compte");

        return $stmt->fetchAll(PDO::FETCH_ASSOC); //retour des résultats sous forme de tableau
    }

    //vérifie si le solde est suffisant pour effectuer une opération
    public function checkSolde($compteId, $montant, $type)
    {
        //requête pour obtenir le solde actuel du compte
        $stmt = $this->pdo->prepare("SELECT Solde, Decouvert FROM Compte WHERE Id_Compte = ?");
        $stmt->execute([$compteId]);
        $compte = $stmt->fetch(PDO::FETCH_ASSOC);
        $decouvert = $compte['Decouvert'];
        $solde = $compte['Solde'];

        //vérifie si le solde est suffisant pour effectuer l'opération
        if ($type === "Retrait" && (int)$solde + (int)$decouvert < $montant) {
            throw new Exception('Solde insuffisant');
        }
    }

    //met à jour le solde du compte après une opération
    public function updateSolde($compteId, $montant, $type)
    {
        if ($type == 'Retrait') {
            //requête pour mettre à jour le solde du compte
            $stmt = $this->pdo->prepare("UPDATE Compte SET Solde = Solde - ? WHERE Id_Compte = ?");
            $stmt->execute([$montant, $compteId]);
        } else if ($type == 'Depot') {
            //requête pour mettre à jour le solde du compte
            $stmt = $this->pdo->prepare("UPDATE Compte SET Solde = Solde + ? WHERE Id_Compte = ?");
            $stmt->execute([$montant, $compteId]);
        }
        
    }

    // Modifie le découvert autorisé du compte
    public function update($compteId, $decouvert)
    {
        //requête pour mettre à jour le découvert autorisé du compte
        $stmt = $this->pdo->prepare("UPDATE Compte SET Decouvert = ? WHERE Id_Compte = ?");
        $stmt->execute([$decouvert, $compteId]);
    }
}
