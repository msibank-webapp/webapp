<?php
class RendezVous {
    protected $pdo; //propriété protégée pour stocker la connexion à la BDD

    //Création d'un nouveau rdv en utilisant une connexion à la BDD
    public function __construct($pdo) {
        $this->pdo = $pdo; //sauvegarde la connexion pour une utilisation ultérieure 
    }

    public function findById ($id) {
        $stmt = $this->pdo->prepare("SELECT * FROM Rendez_vous WHERE Id_Rendezvous = ?");
        $stmt->execute([$id]);
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    //Méthode pour récupérer les rendez-vous d'un conseiller donné sur une période
    public function getAppointmentsForWeek($conseillerId, $weekStart, $weekEnd) {
        // Préparation de la requête SQL pour sélectionner les informations des rendez-vous et des clients associés.
        $stmt = $this->pdo->prepare("SELECT Rendez_vous.Id_Rendezvous, Rendez_vous.Date_debut, Rendez_vous.Date_fin, Client.Id_Client, Client.Prenom, Client.Nom 
        FROM Rendez_vous INNER JOIN Client ON Rendez_vous.Id_Client = Client.Id_Client 
        WHERE Rendez_vous.Id_Conseiller = ? AND Rendez_vous.Date_debut BETWEEN ? AND ?");
        $stmt->execute([$conseillerId, $weekStart, $weekEnd]); //exécution de la requête
        return $stmt->fetchAll(PDO::FETCH_ASSOC); //renvoie des résultats sous forme d'un tableau pour faciliter l'accès aux données 
        //`PDO::FETCH_ASSOC` :le tableau retourné doit utiliser les noms de colonne comme clés. 
        //fetchAll : récupère le jeu de données de la requête executée
    }

    public function getNbRendezvousBetweenDate($startDate, $endDate) {
        $stmt = $this->pdo->prepare("SELECT COUNT(*) AS NbRendezvous FROM Rendez_vous WHERE Date_debut BETWEEN ? AND ?");
        $stmt->execute([$startDate, $endDate]);
        return $stmt->fetchColumn();
    }

    //Méthode pour créer un nouveau rendez-vous
    public function create($conseillerId, $agentId, $motifId, $clientId, $dateDebut, $dateFin) {
        // vérifie que le conseiller n'a pas déjà un rendez-vous à la même date et la même heure
        $stmt = $this->pdo->prepare("SELECT * FROM Rendez_vous WHERE Id_Conseiller = ? AND NOT (Date_fin <= ? OR Date_debut >= ?)");
        $stmt->execute([$conseillerId, $dateDebut, $dateFin]);
        $rdv = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($rdv) {
            throw new Exception("Le conseiller a déjà un rendez-vous ou une tâche administrative à cette date et heure.");
        }

        // même chose pour les tâches administratives 
        $stmt = $this->pdo->prepare("SELECT * FROM Tache_administrative WHERE Id_Conseiller = ? AND NOT (Date_fin <= ? OR Date_debut >= ?)");
        $stmt->execute([$conseillerId, $dateDebut, $dateFin]);
        $tache = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($tache) {
            throw new Exception("Le conseiller a déjà un rendez-vous ou une tâche administrative à cette date et heure.");
        }

        // Préparation de la requête SQL pour insérer un nouveau rendez-vous dans la base de données
        $stmt = $this->pdo->prepare("INSERT INTO Rendez_vous (Id_Conseiller, Id_Agent, Id_Client, Id_Motif, Date_debut, Date_fin) VALUES (?, ?, ?, ?, ?, ?)");
        $stmt->execute([$conseillerId, $agentId, $clientId, $motifId, $dateDebut, $dateFin]); //exécution de la requête
        return $this->pdo->lastInsertId(); //renvoie l'identifiant du dernier enregistrement inséré
    }
}
?>