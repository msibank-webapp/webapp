<?php

class Client
{
    // objet de connexion à la base de données.
    protected $pdo;

    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }
    // Méthode pour obtenir une liste de clients, éventuellement filtrée par un critère de recherche.
    public function getClientList($search = '')
    {
        // Vérifie si la variable 'search' contient une valeur non vide avant de procéder, renvois la liste entière de client si vide.
        // Cela empêche l'exécution de la requête SQL avec un critère de recherche vide, 
        if (!empty($search)) {

            // Prépare une requête SQL qui inclut une condition de recherche sur le prénom ou le nom.
            $stmt = $this->pdo->prepare("SELECT Client.*, Employe.Prenom AS ConseillerPrenom, Employe.Nom AS ConseillerNom FROM Client JOIN Conseiller ON Client.Id_Conseiller = Conseiller.Id_Conseiller JOIN Employe ON Conseiller.Id_Employe = Employe.Id_Employe WHERE Client.Prenom LIKE ? OR Client.Nom LIKE ?");
            $stmt->execute(["%$search%", "%$search%"]);
        } else {
            // Exécute une requête SQL sans filtre de recherche si aucun critère n'est fourni.
            $stmt = $this->pdo->query("SELECT Client.*, Employe.Prenom AS ConseillerPrenom, Employe.Nom AS ConseillerNom FROM Client JOIN Conseiller ON Client.Id_Conseiller = Conseiller.Id_Conseiller JOIN Employe ON Conseiller.Id_Employe = Employe.Id_Employe");
        }

        return $stmt->fetchAll(PDO::FETCH_ASSOC); //renvoie les résultats sous forme de liste
    }

    public function getClientListByConseiller($conseiller_id)
    {
        $stmt = $this->pdo->prepare("SELECT * FROM Client WHERE Id_Conseiller = ?");
        $stmt->execute([$conseiller_id]);

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    // Méthode pour trouver un client spécifique par son ID.
    public function findById($id)
    {
        // Prépare une requête SQL pour obtenir les détails d'un client spécifique, y compris le prénom et le nom de son conseiller.
        $stmt = $this->pdo->prepare("SELECT Client.*, Employe.Prenom AS ConseillerPrenom, Employe.Nom AS ConseillerNom FROM Client JOIN Conseiller ON Client.Id_Conseiller = Conseiller.Id_Conseiller JOIN Employe ON Conseiller.Id_Employe = Employe.Id_Employe WHERE Client.Id_Client = ?");
        $stmt->execute([$id]);

        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    // Méthode pour créer un nouveau client.
    public function createClient($prenom, $nom, $date_de_naissance, $profession, $num_tel, $mail, $adresse, $situation_familliale, $date_creation, $idConseiller)
    {
        // Prépare une requête SQL pour insérer un nouveau client dans la base de données.
        $stmt = $this->pdo->prepare("INSERT INTO Client (Prenom, Nom, Date_de_naissance, Profession, Num_tel, Mail, Adresse, Situation_familliale, Date_creation, Id_Conseiller) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        $stmt->execute([$prenom, $nom, $date_de_naissance, $profession, $num_tel, $mail, $adresse, $situation_familliale, $date_creation, $idConseiller]);

        $stmt = $this->pdo->query("SELECT LAST_INSERT_ID()"); // Récupère l'ID du dernier client inséré.
        return $stmt->fetchColumn(); // Renvoie l'ID du dernier client inséré.
    }

    // Méthode pour mettre à jour les détails d'un client existant.
    public function updateClient($id, $prenom, $nom, $date_de_naissance, $profession, $num_tel, $mail, $adresse, $situation_familliale, $idConseiller)
    {
        // Prépare une requête SQL pour mettre à jour les détails d'un client spécifique.
        $stmt = $this->pdo->prepare("UPDATE Client SET Prenom = ?, Nom = ?, Date_de_naissance = ?, Profession = ?, Num_tel = ?, Mail = ?, Adresse = ?, Situation_familliale = ?, Id_Conseiller = ? WHERE Id_Client = ?");
        $stmt->execute([$prenom, $nom, $date_de_naissance, $profession, $num_tel, $mail, $adresse, $situation_familliale, $idConseiller, $id]);
    }

    public function getNbClientForDate($date)
    {
        $stmt = $this->pdo->prepare("SELECT COUNT(*) AS NbClient FROM Client WHERE Date_creation <= ?");
        $stmt->execute([$date]);
        return $stmt->fetchColumn();
    }
}
