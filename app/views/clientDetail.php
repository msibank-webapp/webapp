<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title>Détails client</title>
    <!-- Lien vers la feuille de style de base pour des styles généraux. -->
    <link rel="stylesheet" href="/public/css/base.css">
    <!-- Lien vers la feuille de style pour la barre de navigation. -->
    <link rel="stylesheet" href="/public/css/navbar.css">
    <!-- Lien vers une feuille de style additionnelle pour des listes. -->
    <link rel="stylesheet" href="/public/css/base-list.css">
    <!-- Importation des icônes de FontAwesome via un CDN. -->
    <script src="https://kit.fontawesome.com/ac37d65e1e.js" crossorigin="anonymous"></script>
    <!-- Importation de polices Google, ici Poppins, utilisées dans la page. -->
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600&display=swap" rel="stylesheet">
</head>

<body>
    <!-- Inclusion du fichier PHP pour la barre de navigation qui est partagée entre les pages. -->
    <?php include dirname(__DIR__) . '/includes/navbar.php'; ?>
    <!-- Un lien pour retourner à la page précédente, avec une icône de flèche gauche. -->
    <a href="javascript:history.back()" class="back-arrow">
        <i class="fa-solid fa-arrow-left"></i>
    </a>
    <!-- Conteneur principal pour les informations du client et des comptes associés. -->
    <div class="div-container">
        <!-- Affichage du nom complet du client. -->
        <h2><?= $client['Prenom'] . ' ' . $client['Nom'] ?></h2>
        <!-- Diverses informations du client telles que date de naissance, etc. -->
        <p>Date de Naissance: <?= $client['Date_de_naissance'] ?></p>
        <p>Date de Création: <?= $client['Date_creation'] ?></p>
        <!-- Plus de détails sur le client. -->
        <p>Profession: <?= $client['Profession'] ?></p>
        <p>Numéro de Téléphone: <?= $client['Num_tel'] ?></p>
        <p>Email: <?= $client['Mail'] ?></p>
        <p>Adresse: <?= $client['Adresse'] ?></p>
        <p>Situation Familiale: <?= $client['Situation_familliale'] ?></p>
        <!-- Affichage du nom du conseiller assigné au client. -->
        <p>Conseiller: <?= $client['ConseillerPrenom'] . ' ' . $client['ConseillerNom'] ?></p>

        <div class="action-btn-container">
            <!-- Lien pour modifier les informaton client. -->
            <a href="/client/update?client_id=<?= $client['Id_Client'] ?>" class="action-btn">Modifier les informations client</a>
        </div>

        <!-- Section pour les comptes du client avec un titre et possibilité d'action si l'utilisateur est un conseiller. -->
        <div class="section-header">
            <h3>Comptes</h3>
            <!-- Condition PHP pour afficher le bouton d'action si l'utilisateur est un conseiller. -->
            <?php if ($_SESSION['role'] === 'Conseiller') : ?> <!-- Si le rôle est "Conseiller" -->
                <div class="action-btn-container">
                    <!-- Lien pour créer un nouveau compte. -->
                    <a href="/compte/create?client_id=<?= $client['Id_Client'] ?>" class="action-btn">Ouvrir un compte</a>
                </div>
            <?php endif; ?>
        </div>
        <!-- Liste des comptes du client avec des détails spécifiques et liens pour plus de détails. -->
        <ul>

            <?php foreach ($comptes as $compte) : ?>
                <li class="item">
                    <div class="item-info">
                        <div class="item-details">
                            <!-- Affichage conditionnel du solde avec style basé sur le montant. -->
                            <span class="<?= $compte['Solde'] > 0 ? 'compte-solde-positive' : 'compte-solde-negative' ?>">
                                <?= $compte['Solde'] ?> €
                            </span>
                            <span> <?= $compte['typeCompte'] ?></span>
                            <!-- Indicateur si le compte est fermé. -->
                            <?php if ($compte['Date_fermeture']) : ?>
                                <span style="color: red;"> - Fermé </span>
                            <?php endif; ?>
                        </div>
                        <!-- Lien pour voir le détail du compte. -->
                        <a class="view-detail" href="/compte?id=<?= $compte['Id_Compte'] ?>">Voir le détail</a>
                    </div>
                </li>
            <?php endforeach; ?>
        </ul>

        <!-- Structure similaire au bloc précédent sur les comptes -->
        <div class="section-header">
            <h3>Contrats</h3>
            <?php if ($_SESSION['role'] === 'Conseiller') : ?>
                <div class="action-btn-container">
                    <a href="/contrat/create?client_id=<?= $client['Id_Client'] ?>" class="action-btn">Nouveau contrat</a>
                </div>
            <?php endif; ?>
        </div>
        <ul>
            <?php foreach ($contrats as $contrat) : ?>
                <li class="item">
                    <div class="item-info">
                        <div class="item-details">
                            <span><?= $contrat['typeContrat'] ?> - <?= $contrat['Libelle'] ?></span>
                            <?php if ($contrat['Date_fermeture']) : ?>
                                <span style="color: red;"> - Fermé </span>
                            <?php endif; ?>
                        </div>
                        <a class="view-detail" href="/contrat?id=<?= $contrat['Id_Contrat'] ?>">Voir le détail</a>
                    </div>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</body>

</html>