<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title>Ajouter un motif de Rendez-vous</title>
    <link rel="stylesheet" href="/public/css/base.css">
    <link rel="stylesheet" href="/public/css/navbar.css">
    <link rel="stylesheet" href="/public/css/base-form.css">
    <script src="https://kit.fontawesome.com/ac37d65e1e.js" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600&display=swap" rel="stylesheet"> <!-- Importe la police Poppins de Google Fonts pour améliorer le style du texte -->
</head>

<body>
    <?php include dirname(__DIR__) . '/includes/navbar.php'; ?> <!-- Inclut la navbar  depuis un autre fichier PHP  -->
    <a href="javascript:history.back()" class="back-arrow"> <!-- Lien pour revenir à la page précédente en utilisant JavaScript -->
        <i class="fa-solid fa-arrow-left"></i> <!-- Icône de flèche gauche de Font Awesome utilisée pour le lien de retour -->
    </a>
    <h1>Nouveau motif de Rendez-vous</h1>
    <form action="/motif/create" method="POST"> <!-- Début du formulaire avec la méthode POST pour envoyer les données au serveur -->

        <label for="typeMotif">Type de motif :</label>
        <select id="typeMotif" name="typeMotif"> <!-- Menu déroulant pour sélectionner le type de motif -->
            <option value="Compte">Compte</option>
            <option value="Contrat">Contrat</option>
            <option value="Autre">Autre</option>
        </select>

        <label for="motif">Nom :</label>
        <input type="text" id="motif" name="motif" required> <!-- Champ de texte pour saisir le nom du motif -->

        <fieldset>
            <legend>Pièces justificatives :</legend>
            <!-- Boucle PHP pour générer une checkbox pour chaque justificatif -->
            <?php foreach ($justificatifs as $justificatif) : ?>
                <div class="checkbox-group">
                    <input type="checkbox" id="justificatif_<?= $justificatif['Id_Piece_a_fournir'] ?>" name="justificatifs[]" value="<?= $justificatif['Id_Piece_a_fournir'] ?>">
                    <label for="justificatif_<?= $justificatif['Id_Piece_a_fournir'] ?>"><?= $justificatif['Nom'] ?></label>
                </div>
            <?php endforeach; ?>
        </fieldset>

        <button type="submit">Valider</button>
    </form>
</body>

</html>