<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title>Nouveau Client</title>
    <!-- Liens vers les feuilles de style pour appliquer le design de base, la barre de navigation et les formulaires. -->
    <link rel="stylesheet" href="/public/css/base.css">
    <link rel="stylesheet" href="/public/css/navbar.css">
    <link rel="stylesheet" href="/public/css/base-form.css">
    <!-- Importation des icônes de FontAwesome via un CDN pour l'utilisation d'icônes stylisées. -->
    <script src="https://kit.fontawesome.com/ac37d65e1e.js" crossorigin="anonymous"></script>
    <!-- Importation de polices Google, ici Poppins, pour un style de texte uniforme et moderne sur la page. -->
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600&display=swap" rel="stylesheet">
</head>

<body>
    <!-- Inclusion du fichier de navigation qui contient la barre de navigation partagée entre les pages. -->
    <?php include dirname(__DIR__) . '/includes/navbar.php'; ?>

    <h1> Nouveau client </h1>
    <!-- Début du formulaire pour la création d'un nouveau client, avec méthode POST pour soumettre les données. -->
    <form action="/client/update" method="POST">
        <!-- Champ caché pour l'ID du client, nécessaire pour identifier le client à mettre à jour. -->
        <input type="hidden" name="id" value="<?= $client['Id_Client'] ?>">

        <label for="prenom">Prenom :</label>
        <input type="text" id="prenom" name="prenom" value="<?= $client['Prenom'] ?>" required>

        <label for="nom">Nom :</label>
        <input type="text" id="nom" name="nom" value="<?= $client['Nom'] ?>" required>

        <label for="date_de_naissance">Date de naissance :</label>
        <input type="date" id="date_de_naissance" name="date_de_naissance" value="<?= $client['Date_de_naissance'] ?>" required>

        <label for="profession">Profession :</label>
        <input type="text" id="profession" name="profession" value="<?= $client['Profession'] ?>" required>

        <label for="num_tel">Numéro de téléphone :</label>
        <input type="text" id="num_tel" name="num_tel" value="<?= $client['Num_tel'] ?>" required>

        <label for="mail">Email :</label>
        <input type="email" id="mail" name="mail" value="<?= $client['Mail'] ?>" required>

        <label for="adresse">Adresse :</label>
        <input type="text" id="adresse" name="adresse" value="<?= $client['Adresse'] ?>" required>

        <label for="situation_familliale">Situation familiale :</label>
        <input type="text" id="situation_familliale" name="situation_familliale" value="<?= $client['Situation_familliale'] ?>" required>

        <label for="conseiller_id">Conseiller :</label>
        <select id="conseiller_id" name="conseillerId">
            <!-- Boucle pour lister tous les conseillers disponibles dans une liste déroulante. -->
            <?php foreach ($conseillers as $conseiller) : ?>
                <option value="<?= $conseiller['Id_Conseiller'] ?>" <?php if ($conseiller['Id_Conseiller'] === $client['Id_Conseiller']) : ?> selected="selected" <?php endif; ?>>
                    <?= $conseiller['Prenom'] . ' ' . $conseiller['Nom'] ?>
                </option>
            <?php endforeach; ?>
        </select>

        <!-- Bouton pour soumettre le formulaire. -->
        <button type="submit">Valider</button>
    </form>
</body>

</html>