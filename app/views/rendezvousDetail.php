<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title>Rendez-vous</title>
    <link rel="stylesheet" href="/public/css/base.css">
    <link rel="stylesheet" href="/public/css/navbar.css">
    <link rel="stylesheet" href="/public/css/base-list.css">
    <script src="https://kit.fontawesome.com/ac37d65e1e.js" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600&display=swap" rel="stylesheet"> <!-- Importation de la police Poppins depuis Google Fonts pour une meilleure typographie -->
</head>

<body>
    <?php include dirname(__DIR__) . '/includes/navbar.php'; ?> <!-- Inclut la navbar  depuis un autre fichier PHP  -->
    <a href="javascript:history.back()" class="back-arrow"><!-- Lien pour revenir à la page précédente en utilisant JavaScript -->
        <i class="fa-solid fa-arrow-left"></i><!-- Icône de flèche gauche de Font Awesome utilisée pour le lien de retour -->
    </a>
    <div class="div-container">
        <h2>Rendez-vous</h2>
        <p>Date : <?= $dateJour ?></p> <!-- Affiche la date du rendez-vous -->
        <p>Heure : <?= $dateHeureDebut ?> - <?= $dateHeureFin ?></p> <!-- Affiche l'heure de début et de fin du rendez-vous -->
        <p>Client : <?= $client['Prenom'] . ' ' . $client['Nom'] ?></p> <!-- Affiche le prénom et le nom du client -->
        <p>Motif : <?= $motif['Nom'] ?></p> <!-- Affiche le nom du motif du rendez-vous -->
        <p>Conseiller: <?= $conseiller['Prenom'] . ' ' . $conseiller['Nom'] ?></p> <!-- Affiche le prénom et le nom du conseiller -->
        <p>Agent: <?= $agent['Prenom'] . ' ' . $agent['Nom'] ?></p> <!-- Affiche le prénom et le nom de l'agent associé -->

        <div class="section-header">
            <h3>Pièces à apporter</h3>
        </div>
        <ul>
            <?php foreach ($justificatifs as $justificatif) : ?>  <!-- Boucle sur chaque pièce justificative associée -->
                <li class="item"> 
                    <div class="item-info"> <!-- Élément individuel listant une pièce justificative -->
                        <div class="item-details"> <!-- Conteneur pour les informations sur la pièce -->
                            <span><?= $justificatif['Nom'] ?></span> <!-- Affiche le nom de la pièce justificative -->
                        </div>
                    </div>
                </li>
            <?php endforeach; ?>
        </ul>

        <div class="section-header">
            <h3>Liens</h3>
        </div>
        <ul>
                <li class="item"> <!-- Élément individuel pour un lien -->
                    <div class="item-info"> <!-- Conteneur pour l'information du lien -->
                        <div class="item-details">
                            <span><a class="view-detail" href="/client?id=<?= $client['Id_Client'] ?>"> Accéder à la fiche client </a></span> <!-- Lien pour accéder à la fiche détaillée du client -->
                        </div>
                    </div>
                </li>

        </ul>
    </div>
</body>

</html>