<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title>Nouvelle opération</title>
    <link rel="stylesheet" href="/public/css/base.css">
    <link rel="stylesheet" href="/public/css/navbar.css">
    <link rel="stylesheet" href="/public/css/base-form.css">
    <script src="https://kit.fontawesome.com/ac37d65e1e.js" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600&display=swap" rel="stylesheet">
</head>

<body>
    <!-- Barre de navigation : Importe la barre de navigation commune pour une utilisation cohérente à travers les pages. -->
    <?php include dirname(__DIR__) . '/includes/navbar.php'; ?>

    <h1>Nouvelle opération</h1>
     <!-- Formulaire pour créer une opération : Soumet les données au serveur via POST pour des raisons de sécurité. -->
    <form action="/operation/create" method="POST">
         <!-- Champ caché, est utilisé pour transmettre l'identifiant du compte concerné sans affichage à l'utilisateur. -->
        <input type="hidden" name="compteId" value="<?= $compteId ?>">

         <!-- Sélection du type d'opération : Permet à l'utilisateur de choisir entre un dépôt et un retrait. -->
        <label for="type">Type d'opération :</label>
        <select id="type" name="type">
            <option value="Depot">Dépôt</option>
            <option value="Retrait">Retrait</option>
        </select>

         <!-- Entrée pour le montant : Accepte uniquement des valeurs numériques non négatives. -->
        <label for="montant">Montant :</label>
        <input type="number" id="montant" name="montant" min="0" step="1" required>

          <!-- Affichage des erreurs, il montre un message d'erreur si une condition d'erreur est détectée dans le traitement. -->
        <?php if (isset($error)) : ?>
            <div class="error-message">
                <?= $error ?>
            </div>
        <?php endif; ?>

         <!-- Bouton pour l'envoie le formulaire pour traitement. -->
        <button type="submit">Valider</button>
    </form>
</body>

</html>