<!DOCTYPE html>
<html lang="fr">

<?php foreach ($actualJustificatifs as $actualJustificatif) :
    error_log($actualJustificatif['Id_Piece_a_fournir']);
endforeach; ?>

<head>
    <meta charset="UTF-8">
    <title>Nouvelle pièce justificative</title>
    <link rel="stylesheet" href="/public/css/base.css">
    <link rel="stylesheet" href="/public/css/navbar.css">
    <link rel="stylesheet" href="/public/css/base-form.css">
    <script src="https://kit.fontawesome.com/ac37d65e1e.js" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600&display=swap" rel="stylesheet"> <!-- Importe la police Poppins de Google Fonts pour améliorer le style du texte -->
</head>

<body>
    <?php include dirname(__DIR__) . '/includes/navbar.php'; ?> <!-- Inclut la navbar  depuis un autre fichier PHP  -->
    <a href="javascript:history.back()" class="back-arrow"> <!-- Lien pour revenir à la page précédente en utilisant JavaScript -->
        <i class="fa-solid fa-arrow-left"></i> <!-- Icône de flèche gauche de Font Awesome utilisée pour le lien de retour -->
    </a>
    <h1>Nouvelle pièce justificative pour <?= $motif['Nom'] ?></h1> <!-- Titre principal de la page, indiquant le motif pour lequel les pièces sont ajoutées -->
    <form action="/motif/link-justificatif" method="POST"> <!-- Début du formulaire pour lier les pièces justificatives au motif -->
        <input type="hidden" name="motif_id" value="<?= $motif['Id'] ?>"> <!-- Champ caché contenant l'ID du motif pour lequel les pièces sont liées -->

        <fieldset>
            <legend>Pièces justificatives :</legend>
            <!-- Boucle PHP pour générer une checkbox pour chaque justificatif -->
            <?php foreach ($justificatifs as $justificatif) :
                $isActualJustificatif = false;
                foreach ($actualJustificatifs as $actualJustificatif) :
                    if ($actualJustificatif['Id_Piece_a_fournir'] === $justificatif['Id_Piece_a_fournir']) :
                        $isActualJustificatif = true;
                        error_log("compare" . $actualJustificatif['Id_Piece_a_fournir'] . " and " . $justificatif['Id_Piece_a_fournir']);
                    endif;
                endforeach; ?>
                <div class="checkbox-group">
                    <input type="checkbox" id="justificatif_<?= $justificatif['Id_Piece_a_fournir'] ?>" name="justificatifs[]" value="<?= $justificatif['Id_Piece_a_fournir'] ?>" <?php if($isActualJustificatif) : echo "checked"; endif; ?>>
                    <label for="justificatif_<?= $justificatif['Id_Piece_a_fournir'] ?>"><?= $justificatif['Nom'] ?></label>
                </div>
            <?php endforeach; ?>
        </fieldset>

        <button type="submit">Valider</button>
    </form>
</body>

</html>