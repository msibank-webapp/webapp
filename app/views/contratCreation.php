<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title>Nouveau contrat</title>
    <link rel="stylesheet" href="/public/css/base.css">
    <link rel="stylesheet" href="/public/css/navbar.css">
    <link rel="stylesheet" href="/public/css/base-form.css">
    <script src="https://kit.fontawesome.com/ac37d65e1e.js" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600&display=swap" rel="stylesheet">
</head>

<body>
    <?php include dirname(__DIR__) . '/includes/navbar.php'; ?>
    <div class="container">
          <!-- Bouton de retour : Permet à l'utilisateur de revenir à la page précédente en utilisant la fonctionnalité 'history.back()' de JavaScript. -->
    <a href="javascript:history.back()" class="back-arrow">
        <i class="fa-solid fa-arrow-left"></i>
    </a>
    <h1>Création d'un contrat Client</h1>

    <!-- Formulaire pour la création d'un contrat : Envoie les données au serveur via POST pour la création du contrat. -->
    <form action="/contrat/create" method="POST">

         <!-- Champ caché : Contient l'ID du client récupéré depuis la chaîne de requête, non visible par l'utilisateur. -->
        <input type="hidden" name="client_id" value="<?= $_GET['client_id'] ?>">
        
        <label for="contratType">Type de contrat :</label>
        <!-- Menu déroulant pour choisir le type de contrat. -->
        <select id="contratType" name="contratType">

            <!-- Boucle PHP pour afficher chaque type de contrat comme option dans le menu déroulant. -->
            <?php foreach ($contratTypes as $contratType) : ?>
                <option value="<?= $contratType['Id_Type_contrat'] ?>"><?= $contratType['Nom'] ?></option>
            <?php endforeach; ?>
        </select>

         <!-- Champ pour le libellé du contrat : Demande à l'utilisateur d'entrer un libellé pour le contrat, requis pour soumettre le formulaire. -->
        <label for="libelle">Libellé :</label>

        <!-- Champ de texte pour entrer le libellé du contrat, marqué comme requis. -->
        <input type="text" id="libelle" name="libelle" required>

        <label for="tarif">Tarif Mensuel :</label>

        <!-- Champ numérique pour le tarif mensuel, avec un minimum de 0 et un pas de 1, requis. -->
        <input type="number" id="tarif" name="tarif" min="0" step="1" required>

        <button type="submit">Créer le contrat</button>
    </form>
    </div>
</body>

</html>