<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title>Pièces justificatives</title>
    <link rel="stylesheet" href="/public/css/base.css">
    <link rel="stylesheet" href="/public/css/navbar.css">
    <link rel="stylesheet" href="/public/css/base-list.css">
    <script src="https://kit.fontawesome.com/ac37d65e1e.js" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600&display=swap" rel="stylesheet"> <!-- Importe la police Poppins de Google Fonts pour améliorer le style du texte -->
</head>

<body>
    <?php include dirname(__DIR__) . '/includes/navbar.php'; ?> <!-- Inclut la navbar  depuis un autre fichier PHP  -->
    <a href="javascript:history.back()" class="back-arrow"> <!-- Lien pour revenir à la page précédente en utilisant JavaScript -->
        <i class="fa-solid fa-arrow-left"></i> <!-- Icône de flèche gauche de Font Awesome utilisée pour le lien de retour -->
    </a>
    <div class="div-container">
        <div class="section-header">
        <h2>Pièces justificatives</h2>
        </div>
        <ul>
            <?php foreach ($justificatifs as $justificatif) : ?> <!-- Boucle PHP pour itérer à travers chaque pièce justificative disponible -->
                <li class="item"> <!-- Élément de liste pour une pièce justificative individuelle -->
                    <div class="item-info"> <!-- Conteneur pour les informations de l'item -->
                        <div class="item-details"> <!-- Détails de l'item spécifique -->
                            <span> <?= $justificatif['Nom'] ?> </span> <!-- Affiche le nom de la pièce justificative -->
                        </div>
                    </div>
                </li>
            <?php endforeach; ?>
        </ul>
        <div class="action-btn-container">
                <a href="/justificatif/create" class="action-btn">Ajouter une pièce justificative</a> <!-- Lien pour ajouter une nouvelle pièce justificative, activant le formulaire de création -->
        </div>
    </div>
</body>

</html>