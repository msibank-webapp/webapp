<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title>Nouveau Client</title>
    <!-- Liens vers les feuilles de style pour appliquer le design de base, la barre de navigation et les formulaires. -->
    <link rel="stylesheet" href="/public/css/base.css">
    <link rel="stylesheet" href="/public/css/navbar.css">
    <link rel="stylesheet" href="/public/css/base-form.css">
     <!-- Importation des icônes de FontAwesome via un CDN pour l'utilisation d'icônes stylisées. -->
    <script src="https://kit.fontawesome.com/ac37d65e1e.js" crossorigin="anonymous"></script>
   <!-- Importation de polices Google, ici Poppins, pour un style de texte uniforme et moderne sur la page. -->
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600&display=swap" rel="stylesheet">
</head>

<body>
     <!-- Inclusion du fichier de navigation qui contient la barre de navigation partagée entre les pages. -->
    <?php include dirname(__DIR__) . '/includes/navbar.php'; ?>

    <h1> Nouveau client </h1>
     <!-- Début du formulaire pour la création d'un nouveau client, avec méthode POST pour soumettre les données. -->
    <form action="/client/create" method="POST">

        <label for="prenom">Prenom :</label>
        <input type="text" id="prenom" name="prenom" require>

        <label for="nom">Nom :</label>
        <input type="text" id="nom" name="nom" require>

        <label for="date_de_naissance">Date de naissance :</label>
        <input type="date" id="date_de_naissance" name="date_de_naissance" require>

        <label for="profession">Profession :</label>
        <input type="text" id="profession" name="profession" require>

        <label for="num_tel">Numéro de téléphone :</label>
        <input type="text" id="num_tel" name="num_tel" require>

        <label for="mail">Email :</label>
        <input type="email" id="mail" name="mail" require>

        <label for="adresse">Adresse :</label>
        <input type="text" id="adresse" name="adresse" require>

        <label for="situation_familliale">Situation familiale :</label>
        <input type="text" id="situation_familliale" name="situation_familliale" require>

         <!-- Condition PHP pour afficher un champ différent basé sur le rôle de l'utilisateur. 
         Si l'utilisateur n'est pas un conseiller, il peut sélectionner un conseiller pour le client. 
         Sinon, l'ID du conseiller est envoyé de manière cachée. -->
        <?php if ($_SESSION['role'] !== "Conseiller") : ?>

            <label for="conseiller_id">Conseiller :</label>
            <select id="conseiller_id" name="conseillerId">
                  <!-- Boucle pour lister tous les conseillers disponibles dans une liste déroulante. -->
                <?php foreach ($conseillers as $conseiller) : ?>
                    <option value="<?= $conseiller['Id_Conseiller'] ?>"><?= $conseiller['Prenom'] . ' ' . $conseiller['Nom'] ?></option>
                <?php endforeach; ?>
            </select>
        <?php else : ?>

            <!-- Champ caché envoyant l'ID du conseiller connecté.-->
            <input type="hidden" name="conseillerId" value="<?= $_SESSION['roleId'] ?>">
        <?php endif; ?>

         <!-- Bouton pour soumettre le formulaire. -->
            <button type="submit">Valider</button>
    </form>
</body>

</html>