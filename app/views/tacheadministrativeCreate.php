<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title>Tâche</title>
    <link rel="stylesheet" href="/public/css/base.css">
    <link rel="stylesheet" href="/public/css/navbar.css">
    <link rel="stylesheet" href="/public/css/base-form.css">
    <script src="https://kit.fontawesome.com/ac37d65e1e.js" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600&display=swap" rel="stylesheet"> <!-- Liste vers police Poppins depuis Google Fonts -->
</head>

<body>
    <?php include dirname(__DIR__) . '/includes/navbar.php'; ?> <!-- Inclut la navbar depuis un autre fichier PHP -->
    <div class="container">
        <a href="javascript:history.back()" class="back-arrow"> <!-- Lien pour revenir à la page précédente -->
            <i class="fa-solid fa-arrow-left"></i> <!-- Icône de flèche gauche pour indiquer l'action de retour -->
        </a>
        <h1>Creer une tâche administrative</h1>
        <form action="/tacheadministrative/create" method="POST"> <!-- Début du formulaire pour la création d'une tâche administrative -->

            <label for="libelle">Libellé:</label>
            <input type="text" id="libelle" name="libelle" required> <!-- Champ de saisie pour le libellé -->

            <label for="dateDebut">Date de début:</label>
            <input type="datetime-local" id="dateDebut" name="dateDebut" required>

            <label for="dateFin">Date de fin:</label>
            <input type="datetime-local" id="dateFin" name="dateFin" required> <!-- type datetime-local pour sélectionner date et heure -->

            <button type="submit">Créer</button>
        </form>
        <?php if (isset($error)) : ?>
            <div class="error-message">
                <?= $error ?>
            </div>
        <?php endif; ?>
    </div>
</body>

</html>