<?php
// Définit le fuseau horaire sur Paris pour asssurer une cohérence des dates
date_default_timezone_set('Europe/Paris');
setlocale(LC_TIME, 'fr_FR.UTF-8', 'French_France.1252'); // Définit les paramètres régionaux en français pour les fonctions de date et de chaîne.
$formatter = new IntlDateFormatter( // Initialise un formatteur de date pour formater les dates en français.
    'fr_FR',
    IntlDateFormatter::FULL, //Format de la date : complet
    IntlDateFormatter::NONE, //Pas de formatage de l'heure
    'Europe/Paris',
    IntlDateFormatter::GREGORIAN //Calendrier Grégorien
);

// Calcule le début de la semaine courante
$weekStart = new DateTime();
$weekStart->setISODate((int)date('o'), (int)date('W') + $weekModifier); //permet de passer d'une semaine à l'autre
$weekStart->modify('Monday this week'); //S'assure que la semaine commence un lundi
// Clone weekStart et ajoute 6 jours pour définir la fin de la semaine.
$weekEnd = clone $weekStart;
$weekEnd->modify('+6 days');

// Crée une période de date pour itérer du début à la fin de la semaine.
$period = new DatePeriod($weekStart, new DateInterval('P1D'), $weekEnd); //dateInterval P1D permet de créer un intervalle d'un jour

// Fonction pour filtrer et trier les tâches ou les rdv par jour
function getItemsForDay($items, $day)
{
    // Filtrage des items dont la date de début correspond au jour donné.
    $filteredItems = array_filter($items, function ($item) use ($day) {
        $result = $day->format('Y-m-d') == (new DateTime($item['Date_debut']))->format('Y-m-d');
        return $result;
    });

    return $filteredItems;
}

// Combine et trie les tâches et rendez-vous par heure de début pour un jour donné.
function combineAndSortItems($tasks, $rendezvous, $day)
{
    // Combine les listes des tâches et rdv
    $combinedItems = array_merge($tasks, $rendezvous);

    //Obtient les tâches et rdv filtrés pour le jour spécifié, regroupé par tâches et par rdv
    $filteredItems = getItemsForDay($combinedItems, $day);

    // trie les items combinés par heure de début
    usort($filteredItems, function ($item1, $item2) {
        $startTime1 = strtotime($item1['Date_debut']);
        $startTime2 = strtotime($item2['Date_debut']);
        return $startTime1 - $startTime2;
    });
    return $filteredItems;
}
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title>Planning</title>
    <link rel="stylesheet" href="/public/css/planning.css">
    <link rel="stylesheet" href="/public/css/base.css">
    <link rel="stylesheet" href="/public/css/navbar.css">
    <script src="https://kit.fontawesome.com/ac37d65e1e.js" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600&display=swap" rel="stylesheet"> <!-- Importation de la famille de polices Poppins de Google Fonts -->
</head>

<body>
    <?php include dirname(__DIR__) . '/includes/navbar.php'; ?> <!-- Inclusion de la navbar depuis un autre fichier PHP -->
    <div class="container">

        <a href="javascript:history.back()" class="back-arrow"> <!-- Lien pour revenir à la page précédente en utilisant JavaScript -->
            <i class="fa-solid fa-arrow-left"></i> <!-- Icône de flèche gauche de Font Awesome utilisée pour le lien -->
        </a>

        <h1>Planning de <?= $conseiller['Prenom'] . ' ' . $conseiller['Nom']; ?></h1>

        <div class="week-navigation">
            <a href="<?= $prevWeekUrl; ?>">&lt; Semaine précédente</a> <!-- Lien pour naviguer à la semaine précédente -->
            <span><?= $formatter->format($weekStart) . ' - ' . $formatter->format($weekEnd); ?></span> <!-- Affichage de la plage de dates actuelle formatée -->
            <a href="<?= $nextWeekUrl; ?>">Semaine suivante &gt;</a> <!-- Lien pour naviguer à la semaine suivante -->
        </div>

        <div class="week-calendar">

            <?php foreach ($period as $day) : ?> <!-- Boucle sur chaque jour de la semaine -->
                <div class="day">
                    <h3><?php echo $formatter->format($day) ?></h3> <!-- Affiche le jour formaté -->
                    <div class="items">
                        <?php
                        $dayItems = combineAndSortItems($taches, $rendezvous, $day);// Recupere la ombinaison et tri des tâches et rendez-vous du jour
                        foreach ($dayItems as $item) : ?> <!-- Boucle sur chaque tâche ou rendez-vous du jour -->
                            <?php
                            $startTime = (new DateTime($item['Date_debut']))->format('H:i'); // Extraction de l'heure de début formatée
                            $endTime = (new DateTime($item['Date_fin']))->format('H:i'); // Extraction de l'heure de fin formatée
                            if (isset($item['Libelle'])) : ?> <!-- Condition pour vérifier si l'élément est une tâche car planning pas de libelle -->
                                <div class="item tache">
                                    <a href="/tacheadministrative/update?id=<?= $item['Id_Tache_administrative'] ?>"> <!-- Lien pour modifier la tâche -->
                                        <strong><?= $startTime ?></strong> <!-- Heure de début de la tâche -->
                                        </br>
                                        <?= $item['Libelle']; ?>
                                        </br>
                                        <strong> <?= $endTime ?></strong> <!-- Heure de fin de la tâche -->
                                    </a>
                                </div>
                            <?php else : ?> <!-- Sinon, l'élément est un rendez-vous -->
                                <div class="item rendezvous">
                                    <a href="/rendezvous/detail?id=<?= $item['Id_Rendezvous'] ?>">
                                        <strong><?= $startTime ?></strong> <!-- Heure de début du rendez-vous -->
                                        </br> Client : <?= $item['Prenom'] . ' ' . $item['Nom'] ?> </br> <!-- Nom du client du rendez-vous -->
                                        <strong> <?= $endTime ?></strong> <!-- Heure de fin du rendez-vous -->
                                    </a>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>

        <?php if ($_SESSION['roleId'] == $conseiller_id) : ?>  <!-- Condition pour vérifier si l'utilisateur actuel est le conseiller -->
            <div class="add-task-button-container">
                <a href="/tacheadministrative/create" class="add-task-button">Ajouter une tâche</a> <!-- Lien pour ajouter une nouvelle tâche -->
            </div>
        <?php endif; ?>
        <?php if ($_SESSION['role'] === "Agent") : ?> <!-- Condition pour vérifier si l'utilisateur est un agent -->
            <div class="add-task-button-container"> 
                <a href="/rendezvous/create?conseiller_id=<?= $conseiller_id ?>" class="add-task-button">Nouveau Rendez-vous</a> <!-- Lien pour ajouter un nouveau rendez-vous -->
            </div>
        <?php endif; ?>
    </div>
</body>

</html>