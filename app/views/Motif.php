<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title>Comptes et Contrats</title>
    <link rel="stylesheet" href="/public/css/base.css">
    <link rel="stylesheet" href="/public/css/navbar.css">
    <link rel="stylesheet" href="/public/css/base-list.css">
    <script src="https://kit.fontawesome.com/ac37d65e1e.js" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600&display=swap" rel="stylesheet"> <!-- Importe la police Poppins de Google Fonts pour améliorer le style du texte -->
</head>

<body>
    <?php include dirname(__DIR__) . '/includes/navbar.php'; ?> <!-- Inclut la navbar  depuis un autre fichier PHP  -->
    <a href="javascript:history.back()" class="back-arrow"> <!-- Lien pour revenir à la page précédente en utilisant JavaScript -->
        <i class="fa-solid fa-arrow-left"></i> <!-- Icône de flèche gauche de Font Awesome utilisée pour le lien de retour -->
    </a>
    <div class="div-container">
        <div class="section-header">
            <h2>Comptes et contrats</h2>
        </div>

        <div class="section-header">
            <h3>Comptes</h3>
        </div>
        <ul>
            <?php foreach ($typeComptes as $typeCompte) : ?> <!-- Boucle à travers chaque type de compte -->
                <li class="item">
                    <div class="item-info">
                        <div class="item-details">
                            <span> <a class="view-detail" href="/motif?id=<?= $typeCompte['Id_Motif'] ?>"> <?= $typeCompte['Nom'] ?> </a></span> <!-- Lien vers la page de détail du type de compte -->
                        </div>
                    </div>
                </li>
            <?php endforeach; ?>
        </ul>


        <div class="section-header">
            <h3>Contrats</h3> <!-- En-tête de la section pour "Contrats" -->
        </div>
        <ul>
            <?php foreach ($typeContrats as $typeContrat) : ?>  <!-- Boucle à travers chaque type de contrat --> 
                <li class="item">
                    <div class="item-info">
                        <div class="item-details">
                            <span> <a class="view-detail" href="/motif?id=<?= $typeContrat['Id_Motif'] ?>"><?= $typeContrat['Nom'] ?></a></span> <!-- Lien vers la page de détail du type de contrat -->
                        </div>
                    </div>
                </li>
            <?php endforeach; ?>
        </ul>

        <div class="section-header">
            <h3>Autres motifs</h3>
        </div>
        <ul>
            <?php foreach ($otherMotifs as $otherMotif) : ?> <!-- Boucle à travers chaque autre motif -->
                <li class="item-item">
                    <div class="item-info">
                        <div class="item-details">
                            <span><a class="view-detail" href="/motif?id=<?= $otherMotif['Id_Motif'] ?>"> <?= $otherMotif['Libelle'] ?> </a></span> <!-- Lien vers la page de détail de l'autre motif -->
                        </div>
                    </div>
                </li>
            <?php endforeach; ?>
        </ul>
        <div class="action-btn-container">
            <a href="/motif/create" class="action-btn">Nouveau motif de Rendez-vous</a> <!-- Lien pour créer un nouveau motif de rendez-vous -->
        </div>
        </div>
    </div>
</body>

</html>