<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title>Conseillers</title>
    <link rel="stylesheet" href="/public/css/base.css">
    <link rel="stylesheet" href="/public/css/navbar.css">
    <link rel="stylesheet" href="/public/css/base-list.css">
    <script src="https://kit.fontawesome.com/ac37d65e1e.js" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600&display=swap" rel="stylesheet"> <!-- Charge la police Poppins de Google Fonts -->
</head>

<body>
    <?php include dirname(__DIR__) . '/includes/navbar.php'; ?> <!-- Inclut la navbar depuis un autre fichier PHP -->
    <a href="javascript:history.back()" class="back-arrow"> <!-- Lien pour retourner à la page précédente--->
        <i class="fa-solid fa-arrow-left"></i> <!-- Icône de flèche gauche de Font Awesome pour le lien de retour -->
    </a>
    <div class="div-container">
        <div class="section-header">
            <h2>Afficher le planning d'un conseiller</h2>
        </div>
        <ul> <!-- Liste pour afficher les conseillers -->
            <?php foreach ($conseillers as $conseiller) : ?>  <!-- Boucle à travers chaque conseiller récupéré -->
                <li class="item">
                    <div class="item-info">
                        <div class="item-details">
                            <span> <a class="view-detail" href="/planning?conseiller_id=<?= $conseiller['Id_Conseiller'] ?>"> <?= $conseiller['Prenom'] . ' ' . $conseiller['Nom'] ?> </a> </span> <!-- Lien pour voir le planning du conseiller avec le nom comme texte du lien -->
                        </div>
                    </div>
                </li>
            <?php endforeach; ?>
        </ul>
</body>

</html>