<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title>Détails client</title>
    <!-- Liens vers les feuilles de style pour appliquer le design de base, la barre de navigation et les formulaires. -->
    <link rel="stylesheet" href="/public/css/base.css">
    <link rel="stylesheet" href="/public/css/navbar.css">
    <link rel="stylesheet" href="/public/css/base-form.css">
    <script src="https://kit.fontawesome.com/ac37d65e1e.js" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600&display=swap" rel="stylesheet">
</head>

<body>
    <!-- Inclusion du fichier de navigation qui contient la barre de navigation partagée entre les pages. -->
    <?php include dirname(__DIR__) . '/includes/navbar.php'; ?>
    
    <!-- Conteneur principal pour le formulaire et les éléments interactifs de la page. -->
    <div class="container">
     <!-- Lien pour retourner à la page précédente, avec une icône de flèche gauche. -->
    <a href="javascript:history.back()" class="back-arrow">
        <i class="fa-solid fa-arrow-left"></i>
    </a>
    <h1>Création d'un compte Client</h1>
     <!-- Début du formulaire pour la création d'un nouveau compte client, avec méthode POST pour soumettre les données. -->
    <form action="/compte/create" method="POST">
        
         <!-- Champ caché pour passer l'ID du client récupéré via GET à la soumission du formulaire. -->
        <input type="hidden" name="client_id" value="<?= $_GET['client_id'] ?>">

        <!-- Sélection du type de compte avec une liste déroulante remplie par les types disponibles. -->
        <label for="compteType">Type de compte :</label>
        <select id="compteType" name="compteType">
             <!-- Boucle pour lister tous les types de compte disponibles dans une liste déroulante. -->
            <?php foreach ($compteTypes as $compteType) : ?>
                <option value="<?= $compteType['Id_Type_compte'] ?>"><?= $compteType['Nom'] ?></option>
            <?php endforeach; ?>
        </select>

         <!-- Champ pour saisir le découvert autorisé, avec un contrôle sur la valeur minimale. -->
        <label for="decouvert">Découvert autorisé :</label>
        <input type="number" id="decouvert" name="decouvert" min="0" required>

         <!-- Champ pour saisir l'acompte de départ avec un contrôle sur la valeur minimale et les pas de montant. -->
        <label for="solde">Acompte de départ :</label>
        <input type="number" id="solde" name="solde" min="0" step="1" required>

         <!-- Bouton pour soumettre le formulaire. -->
        <button type="submit">Créer le compte</button>
    </form>
    </div>
</body>

</html>