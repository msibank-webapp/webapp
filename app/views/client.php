<?php
$role = $_SESSION['role'];
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title>Clients</title>
    <link rel="stylesheet" href="/public/css/base.css">
    <link rel="stylesheet" href="/public/css/navbar.css">
    <link rel="stylesheet" href="/public/css/client.css">
    <script src="https://kit.fontawesome.com/ac37d65e1e.js" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600&display=swap" rel="stylesheet">
</head>

<body>
    <?php include dirname(__DIR__) . '/includes/navbar.php'; ?>
    <h1>Clients</h1>
    <div class="search-container">
        <div class="search-bar">
            <input type="text" class="search-input" id="search-input" dockeclass="search-input" placeholder="Rechercher...">
            <button class="search-btn" disabled> <!-- Bouton désactivé par défaut via l'attribut "disabled"-->
                <i class="fas fa-search"></i> <!-- Sert à embellir le bouton visuellement -->
            </button>
        </div>
        <a href="/client/create" class="new-client-btn">Nouveau Client</a> <!--permet de naviguer vers une page pour créer un nouveau client-->
    </div>

    <ul class="client-list">
        <?php foreach ($clients as $client) : ?>
            <li class="client-item">
                <a href="/client?id=<?= $client['Id_Client'] ?>">
                    <ul class="client-info"> <!-- Créer une liste -->
                        <li class="client-name"> <?= $client['Prenom'] . ' ' . $client['Nom'] ?> </li>
                        <li class="client-conseiller">Conseiller: <?= $client['ConseillerPrenom'] ?> <?= $client['ConseillerNom'] ?> </li>
                        <li class="client-tel">Numéro de téléphone: <?= $client['Num_tel'] ?></li>
                        <li class="client-mail">Email: <?= $client['Mail'] ?></li>
                    </ul>
                </a>
            </li>
        <?php endforeach; ?> <!-- fin de la recherche -->
    </ul>
</body>

<script>
    document.getElementById('search-input').addEventListener('keypress', function(event) { //ajouter un évenement sur l'élément input pour la recherche
        if (event.key === 'Enter') {
            window.location.href = '/client?search=' + this.value; //Redirection vers la page de l'URL, affichage du champ de recherche avec la valeur saisie

        }
    });
</script>

</html>