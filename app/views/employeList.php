<!DOCTYPE html> <!--affiche une liste d'employés et permet d'ajouter un nouvel employé -->
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title>Employés</title>
    <link rel="stylesheet" href="/public/css/base.css">
    <link rel="stylesheet" href="/public/css/navbar.css">
    <link rel="stylesheet" href="/public/css/base-list.css">
    <script src="https://kit.fontawesome.com/ac37d65e1e.js" crossorigin="anonymous"></script> <!-- pour récuperer les icones -->
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600&display=swap" rel="stylesheet"> <!-- police d'écriture -->
</head>

<body>
    <?php include dirname(__DIR__) . '/includes/navbar.php'; ?> <!-- il inclue le fichier de la navbar -->
    <a href="javascript:history.back()" class="back-arrow">
        <i class="fa-solid fa-arrow-left"></i>
    </a>
    <div class="div-container">
        <div class="section-header">
            <h2>Employés</h2> <!--titre de la liste-->
            <div class="action-btn-container"> 
                <a href="/employe/create" class="action-btn">Ajouter un employé</a><!--affichage du bouton pour ajouter un nouvel employé -->
            </div>
        </div>

        <ul>
            <?php foreach ($employes as $employe) : ?>
                <li class="item">
                    <div class="item-info">
                        <div class="item-details">
                            <span> <a class="view-detail" href="/employe/update?id=<?= $employe['Id_Employe'] ?>"> <?= $employe['Prenom'] . ' ' . $employe['Nom'] ?> </a> </span>
                        </div>
                    </div>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</body>

</html>