<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title>Modifier les informations du compte</title>
    <link rel="stylesheet" href="/public/css/base.css">
    <link rel="stylesheet" href="/public/css/navbar.css">
    <link rel="stylesheet" href="/public/css/base-form.css">
    <script src="https://kit.fontawesome.com/ac37d65e1e.js" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600&display=swap" rel="stylesheet">
</head>

<body>
    <?php include dirname(__DIR__) . '/includes/navbar.php'; ?>
    <!-- Lien pour retourner à la page précédente, utilisant JavaScript pour manipuler l'historique de navigation du navigateur. -->
    <a href="javascript:history.back()" class="back-arrow">
        <i class="fa-solid fa-arrow-left"></i>
    </a>

     <!-- Titre de la page : Affiche le prénom et le nom du client ainsi que le type de compte pour identification. -->
    <h1><?= $compte['prenomClient'] . ' ' . $compte['nomClient'] . ' - ' . $compte['typeCompte'] ?> </h1>
    
    <!-- Formulaire de mise à jour des informations du compte : Permet la modification du montant de découvert autorisé. -->
    <form action="/compte/update" method="POST">
         <!-- Champ caché pour transporter l'identifiant du compte sans le montrer à l'utilisateur. -->
        <input type="hidden" name="compteId" value="<?= $compte['Id_Compte'] ?>">
        
         <!-- Entrée pour la modification du découvert autorisé : Accepte seulement des valeurs numériques positives. -->
        <label for="decouvert">Découvert autorisé :</label>
        <input type="number" id="decouvert" name="decouvert" min="0" step="1" required>
        
         <!-- Bouton pour soumettre le formulaire après la mise à jour des informations. -->
        <button type="submit">Valider</button>
    </form>
</body>

</html>