<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title>Tâche</title>
    <link rel="stylesheet" href="/public/css/base.css">
    <link rel="stylesheet" href="/public/css/navbar.css">
    <link rel="stylesheet" href="/public/css/base-form.css">
    <script src="https://kit.fontawesome.com/ac37d65e1e.js" crossorigin="anonymous"></script> <!-- Charge les icônes de Font Awesome -->
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600&display=swap" rel="stylesheet"> <!-- Charge la police de caractères Poppins depuis Google Fonts -->
</head>

<body>
    <?php include dirname(__DIR__) . '/includes/navbar.php'; ?> <!-- Inclut la navbar depuis un autre fichier PHP -->
    <div class="container">
        <a href="javascript:history.back()" class="back-arrow"> <!-- Lien pour retourner à la page précédente -->
            <i class="fa-solid fa-arrow-left"></i> <!-- Icône de flèche gauche pour le lien de retour -->
        </a>
        <h1>Editer une tâche administrative</h1>
            <form action="/tacheadministrative/update" method="POST"> <!-- Formulaire pour envoyer les données mises à jour -->
                <input type="hidden" name="id" value="<?= $task['Id_Tache_administrative'] ?>"> <!-- Champ caché pour l'ID de la tâche -->
                <input type="hidden" name="conseiller_id" value="<?= $task['Id_Conseiller'] ?>"> <!-- Champ caché pour l'ID du conseiller -->

                    <label for="libelle">Libellé:</label>
                    <input type="text" id="libelle" name="libelle" value="<?= $task['Libelle'] ?>"> <!-- Champ de texte pour le libellé avec valeur pré-remplie -->

                    <label for="dateDebut">Date de début:</label>
                    <input type="datetime-local" id="dateDebut" name="dateDebut" value="<?= (new DateTime($task['Date_debut']))->format('Y-m-d\TH:i') ?>">

                    <label for="dateFin">Date de fin:</label>
                    <input type="datetime-local" id="dateFin" name="dateFin" value="<?= (new DateTime($task['Date_fin']))->format('Y-m-d\TH:i') ?>"> <!-- type datetime-local pour sélectionner date et heure -->

                <button type="submit"> Valider </button>
            </form>
            <?php if (isset($error)) : ?>
                <div class="error-message">
                    <?= $error ?>
                </div>
            <?php endif; ?>

        <form method="POST" action="/tacheadministrative/delete"> <!-- Formulaire pour supprimer la tâche -->
            <input type="hidden" name="id" value="<?= $task['Id_Tache_administrative'] ?>"> <!-- Champ caché pour l'ID de la tâche -->
            <button type="submit">Supprimer</button>
        </form>
    </div>
</body>

</html>