<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title>Tâche</title>
    <link rel="stylesheet" href="/public/css/base.css">
    <link rel="stylesheet" href="/public/css/navbar.css">
    <link rel="stylesheet" href="/public/css/base-form.css">
    <script src="https://kit.fontawesome.com/ac37d65e1e.js" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600&display=swap" rel="stylesheet"><!-- Chargement de la police Poppins depuis Google Fonts -->
</head>

<body>
    <?php include dirname(__DIR__) . '/includes/navbar.php'; ?> <!-- Inclut la navbar depuis un autre fichier PHP -->
    <div class="container">
        <a href="javascript:history.back()" class="back-arrow"> <!-- Lien pour revenir à la page précédente -->
            <i class="fa-solid fa-arrow-left"></i> <!-- Icône de flèche gauche pour le retour -->
        </a>
        <h1>Nouveau Rendez-vous</h1>
        <form action="/rendezvous/create" method="POST"> <!-- Début du formulaire pour la création d'un rendez-vous -->
            <input type="hidden" name="conseiller_id" value="<?= $conseiller_id ?>">  <!-- Champ caché pour passer l'ID du conseiller -->
            <input type="hidden" name="agent_id" value="<?= $_SESSION['roleId'] ?>"> <!-- Champ caché pour passer l'ID de l'agent connecté -->

            <label for="client_id">Client :</label>
            <select id="client_id" name="client_id"> <!-- Menu déroulant pour choisir un client -->
                <?php foreach ($clients as $client) : ?> <!-- Boucle pour remplir le menu déroulant avec les clients -->
                    <option value="<?= $client['Id_Client'] ?>"><?= $client['Prenom'] . ' ' . $client['Nom'] ?></option>
                <?php endforeach; ?>
            </select>

            <label for="motif_Id">Motif :</label>
            <select id="motif_Id" name="motif_id"> <!-- Menu déroulant pour choisir le motif -->
                <?php foreach ($motifs as $motif) : ?> <!-- Boucle pour remplir le menu déroulant avec les motifs disponibles -->
                    <option value="<?= $motif['Id'] ?>"><?= $motif['Nom'] ?></option>
                <?php endforeach; ?>
            </select>

            <label for="dateDebut">Date de début:</label>
            <input type="datetime-local" id="dateDebut" name="dateDebut" required> <!-- Champ pour saisir la date et l'heure de début, requis -->

            <label for="dateFin">Date de fin:</label>
            <input type="datetime-local" id="dateFin" name="dateFin" required>

            <button type="submit"> Créer </button>
            <?php if (isset($error)) : ?>
                <div class="error-message">
                    <?= $error ?>
                </div>
            <?php endif; ?>
        </form>
    </div>
</body>

</html>