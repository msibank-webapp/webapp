<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title>Détails contrat</title>
    <link rel="stylesheet" href="/public/css/base.css">
    <link rel="stylesheet" href="/public/css/navbar.css">
    <link rel="stylesheet" href="/public/css/base-list.css">
    <script src="https://kit.fontawesome.com/ac37d65e1e.js" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600&display=swap" rel="stylesheet">
</head>

<body>
    <?php include dirname(__DIR__) . '/includes/navbar.php'; ?>
    <a href="javascript:history.back()" class="back-arrow">
        <i class="fa-solid fa-arrow-left"></i>
    </a>
    <!-- Conteneur pour les détails du contrat, structurant la mise en page. -->
    <div class="div-container">
        <h2><?= $contrat['prenomClient'] . ' ' . $contrat['nomClient'] ?></h2>
        <p>Libellé: <?= $contrat['Libelle'] ?></p>
        <p>Type de contrat: <?= $contrat['typeContrat'] ?></p>
        <p>Tarif mentuel: <?= $contrat['Tarif_mensuel'] ?> €</p>
        <p>Date d'ouverture: <?= $contrat['Date_ouverture'] ?></p>
        <!-- Condition PHP vérifiant si la date de fermeture du contrat n'est pas définie. -->
        <?php if (!$contrat['Date_fermeture']) : ?>
        <!-- Si le contrat est fermé, affiche la date de clôture. -->
        <p>Date de clôture : <?= $contrat['Date_fermeture'] ?></p>
        <?php endif; ?> 
        <!-- Affiche le statut du contrat, indiquant si le contrat est clôturé ou toujours en cours. -->   
        <p>Statut du contrat: <?= $contrat['Date_fermeture'] ? 'Cloturé' : 'En cours' ?></p>

        <!-- Condition PHP pour afficher l'option de clôture du contrat si le contrat est ouvert et si l'utilisateur est un conseiller. -->
        <?php if (!$contrat['Date_fermeture'] && $_SESSION['role'] === 'Conseiller') : ?>
        <div class="section-header">
            <div class="action-btn-container"> <!-- Conteneur pour le bouton d'action. -->
            
            <!-- Lien pour clôturer le contrat, envoie les identifiants du contrat et du client comme paramètres GET. -->
                <a href="/contrat/close?contrat_id=<?= $contrat['Id_Contrat'] ?>&client_id=<?= $contrat['Id_Client'] ?>" class="action-btn">Clôturer le contrat</a>
            </div>
        </div>
        <?php endif; ?>
    </div>
</body>

</html>