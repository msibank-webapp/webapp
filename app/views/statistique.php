<!DOCTYPE html> <!--affiche des statistiques générées pour une période donnée -->
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Rendez-vous</title>
    <link rel="stylesheet" href="/public/css/base.css">
    <link rel="stylesheet" href="/public/css/navbar.css">
    <link rel="stylesheet" href="/public/css/base-list.css">
    <script src="https://kit.fontawesome.com/ac37d65e1e.js" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600&display=swap" rel="stylesheet">
</head>

<body>
    <?php include dirname(__DIR__) . '/includes/navbar.php'; ?>
    <a href="javascript:history.back()" class="back-arrow">
        <i class="fa-solid fa-arrow-left"></i>
    </a>
    <div class="div-container">
        <div class="section-header">
            <h2>Statistiques générées</h2>
        </div>
        <ul>
            <li class="item">
                <div class="item-info">
                    <div class="item-details">
                        <span> Nombre de contrats souscrits entre le <?= $startDate . ' et le ' . $endDate ?> : <?= $statistiques['NbContrat'] ?> </span>
                    </div>
                </div>
            </li>
            <li class="item">
                <div class="item-info">
                    <div class="item-details">
                        <span> Nombre de rendez-vous pris entre le <?= $startDate . ' et le ' . $endDate ?> : <?= $statistiques['NbRendezvous'] ?> </span>
                    </div>
                </div>
            </li>
            <li class="item">
                <div class="item-info">
                    <div class="item-details">
                        <span> Nombre de client à date du <?= $dateClientCount ?> : <?= $statistiques['NbClient'] ?> </span>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</body>