<?php
$role = $_SESSION['role']; //celle definie dans authenticate.php
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title>Accueil</title>
    <link rel="stylesheet" href="/public/css/base.css">
    <link rel="stylesheet" href="/public/css/navbar.css">
    <script src="https://kit.fontawesome.com/ac37d65e1e.js" crossorigin="anonymous"></script> <!-- pour récuperer les icones -->
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600&display=swap" rel="stylesheet"> <!-- police d'écriture -->
</head>

<body>
    <?php include dirname(__DIR__) . '/includes/navbar.php'; ?> <!-- il inclue le fichier de la navbar -->
    <h1>Bienvenue !</h1>
    <p>Votre rôle est : <strong><?php echo($role); ?></strong></p> <!-- recuperation du role pour ensuite l'afficher -->
</body>
</html>