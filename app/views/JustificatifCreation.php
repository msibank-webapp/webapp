<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title>Nouvelle pièce justificative</title>
    <link rel="stylesheet" href="/public/css/base.css">
    <link rel="stylesheet" href="/public/css/navbar.css">
    <link rel="stylesheet" href="/public/css/base-form.css">
    <script src="https://kit.fontawesome.com/ac37d65e1e.js" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600&display=swap" rel="stylesheet"> <!-- Importe la police Poppins de Google Fonts pour améliorer le style du texte -->
</head>

<body>
    <?php include dirname(__DIR__) . '/includes/navbar.php'; ?> <!-- Inclut la navbar  depuis un autre fichier PHP  -->
    <a href="javascript:history.back()" class="back-arrow"> <!-- Lien pour revenir à la page précédente en utilisant JavaScript -->
        <i class="fa-solid fa-arrow-left"></i> <!-- Icône de flèche gauche de Font Awesome utilisée pour le lien de retour -->
    </a>
    <h1>Nouvelle pièce justificative</h1>
    <form action="/justificatif/create" method="POST"> <!-- Début du formulaire pour la création d'une nouvelle pièce justificative -->

        <label for="name">Pièce à fournir :</label>
        <input type="text" id="name" name="name"> <!-- Champ de saisie pour le nom de la pièce justificative -->

        <button type="submit">Valider</button> <!-- Bouton pour soumettre le formulaire, envoie les données au serveur -->
    </form>
</body>

</html>