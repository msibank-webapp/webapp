<!DOCTYPE html> <!--affiche un formulaire pour générer des statistiques en fonction de certaines dates-->
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Rendez-vous</title>
    <link rel="stylesheet" href="/public/css/base.css">
    <link rel="stylesheet" href="/public/css/navbar.css">
    <link rel="stylesheet" href="/public/css/base-form.css">
    <script src="https://kit.fontawesome.com/ac37d65e1e.js" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600&display=swap" rel="stylesheet">
</head>
<body>
    <?php include dirname(__DIR__) . '/includes/navbar.php'; ?>

    <h1>Statistiques</h1>
    <form action="/statistique/generate" method="POST">

            <label for="startDate">Date de début :</label>
            <input type="date" id="startDate" name="startDate" required>

            <label for="endDate">Date de fin :</label>
            <input type="date" id="endDate" name="endDate" required>

            <label for="onDate">Nombre de client à date :</label>
            <input type="date" id="onDate" name="onDate" required>

        <button type="submit">Generate Statistics</button>
    </form>