<!DOCTYPE html> <!-- formulaire pour modifier les informations de connexion d'un employé spécifique -->
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title>Modifier les informations de connexion</title>
    <link rel="stylesheet" href="/public/css/base.css">
    <link rel="stylesheet" href="/public/css/navbar.css">
    <link rel="stylesheet" href="/public/css/base-form.css">
    <script src="https://kit.fontawesome.com/ac37d65e1e.js" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600&display=swap" rel="stylesheet">
</head>

<body>
    <?php include dirname(__DIR__) . '/includes/navbar.php'; ?>
    <a href="javascript:history.back()" class="back-arrow">
        <i class="fa-solid fa-arrow-left"></i>
    </a>
    <h1><?= $employe['Prenom'] . ' ' . $employe['Nom'] . ' - ' . $employe['Role'] ?> </h1> <!--affiche ensuite un titre avec le nom et le rôle de l'employé -->
    <form action="/employe/update" method="POST">
        <input type="hidden" name="employeId" value="<?= $employe['Id_Employe'] ?>">

        <label for="login">Identifiant :</label>
        <input type="text" id="login" name="login" value="<?= $employe['Login'] ?>">

        <label for="password">Mot de passe :</label>
        <input type="password" id="password" name="password">

        <button type="submit">Valider</button>
    </form>
</body>

</html>