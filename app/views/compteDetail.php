<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title>Détails compte</title>
    <link rel="stylesheet" href="/public/css/base.css">
    <link rel="stylesheet" href="/public/css/navbar.css">
    <link rel="stylesheet" href="/public/css/base-list.css">
    <script src="https://kit.fontawesome.com/ac37d65e1e.js" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600&display=swap" rel="stylesheet">
</head>

<body>
     <!-- Inclusion du fichier de navigation qui contient la barre de navigation partagée entre les pages. -->
    <?php include dirname(__DIR__) . '/includes/navbar.php'; ?>
     <!-- Lien pour retourner à la page précédente, avec une icône de flèche gauche. -->
    <a href="javascript:history.back()" class="back-arrow">
        <i class="fa-solid fa-arrow-left"></i>
    </a>
      <!-- Conteneur principal pour les informations du compte et des opérations associées. -->
    <div class="div-container">

         <!-- Affichage du nom du client associé au compte. -->
        <h2><?= $compte['prenomClient'] . ' ' . $compte['nomClient'] ?></h2>

         <!-- Détails du compte tel que le type de compte et le solde actuel. -->
        <p>Type de compte: <?= $compte['typeCompte'] ?></p>
        <p>Solde: <?= $compte['Solde'] ?> €</p>
        <p>Découvert autorisé: <?= $compte['Decouvert'] ?> €</p>
        <p>Date d'ouverture: <?= $compte['Date_ouverture'] ?></p>

         <!-- Condition pour afficher la date de clôture si le compte est fermé. -->
        <?php if ($compte['Date_fermeture']) : ?>
            <p>Date de clôture : <?= $compte['Date_fermeture'] ?></p>
        <?php endif; ?>

         <!-- Affichage du statut actuel du compte, dépendant de la présence d'une date de fermeture. -->
        <p>Statut du compte: <?= $compte['Date_fermeture'] ? 'Fermé' : 'Ouvert' ?></p>


        <!-- Section pour les opérations du compte, affichée uniquement si le compte est ouvert. -->
        <?php if (!$compte['Date_fermeture']) : ?>
            <div class="section-header">
                <h3>Opérations</h3>

                <!-- Bouton pour ajouter une nouvelle opération si le compte est ouvert. -->
                <div class="action-btn-container">
                    <a href="/operation/create?compte_id=<?= $compte['Id_Compte'] ?>" class="action-btn">Nouvelle opération</a>
                </div>
            </div>

             <!-- Liste des opérations effectuées sur le compte avec détails comme le montant et le type d'opération. -->
        <?php endif; ?>
        <ul>
            <?php foreach ($operations as $operation) : ?>
                <li class="item">
                    <div class="item-info">
                        <div class="item-details">
                            <!-- Style conditionnel pour afficher le montant en vert pour les dépôts et en rouge pour les retraits. -->
                            <span class="<?= $operation['Type_operation'] == "Depot" ? 'operation-positive' : 'operation-negative' ?>">
                                <?= $operation['Montant'] ?> €
                            </span>
                            <span> <?= $operation['Type_operation'] ?></span>
                        </div>
                    </div>
                </li>
            <?php endforeach; ?>
        </ul>

          <!-- Bouton pour fermer le compte, visible uniquement pour les conseillers si le compte est ouvert. -->
        <?php if (!$compte['Date_fermeture'] && $_SESSION['role'] === 'Conseiller') : ?>
            <div class="action-btn-container">
                <a href="/compte/close?compte_id=<?= $compte['Id_Compte'] ?>&client_id=<?= $compte['Id_Client'] ?>" class="action-btn">Fermer le compte</a>
            </div>
        <?php endif; ?>

         <!-- Bouton pour modifier le découvert autorisé, disponible seulement si le compte est ouvert. -->
        <?php if (!$compte['Date_fermeture']) : ?>
        <div class="action-btn-container">
            <a href="/compte/update?compte_id=<?= $compte['Id_Compte'] ?>" class="action-btn">Modifier le découvert</a>
        </div>
        <?php endif; ?>
    </div>
</body>

</html>