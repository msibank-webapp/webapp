<?php
// Inclusion du modèle Employé pour pouvoir utiliser ses fonctions.
require_once dirname(__DIR__) . '/models/Employe.php';

// Déclaration de la classe EmployeManagementController pour gérer les opérations liées aux employés.
Class EmployeManagementController {
    // Propriété pour stocker une instance du modèle Employe.
    protected $employeModel;

    // Constructeur qui initialise le modèle Employe avec une connexion PDO passée en paramètre.
    public function __construct($pdo) {
        // Assignation de l'instance de modèle Employe à la propriété de la classe.
        $this->employeModel = new Employe($pdo);
    }

    // Méthode pour récupérer et afficher la liste de tous les employés.
    public function EmployeList() {
        // Utilisation de la méthode findAll du modèle pour récupérer tous les employés.
        $employes = $this->employeModel->findAll();

        // Inclusion de la vue qui affiche la liste des employés.
        require dirname(__DIR__) . '/views/employeList.php';
    }

     // Méthode pour éditer les informations d'un employé spécifique en utilisant son ID.
    public function EditEmploye($id) {
        // Utilisation de la méthode findById du modèle pour récupérer un employé spécifique par son ID.
        $employe = $this->employeModel->findById($id);

         // Inclusion de la vue pour la mise à jour des informations de l'employé.
        require dirname(__DIR__) . '/views/employeUpdate.php';
    }

    // Méthode pour éditer les identifiants de connexion d'un employé.
    public function EditCredentials() {
         // Récupération de l'ID de l'employé et de ses nouveaux identifiants de connexion via POST.
        $id = $_POST['employeId'];
        $login = $_POST['login'];
        $password = $_POST['password'];

         // Appel à la méthode editCredentials du modèle pour mettre à jour les identifiants de connexion.
        $this->employeModel->editCredentials($id, $login, $password);

         // Redirection vers la page de liste des employés après mise à jour.
        header('Location: /employe');
    }

    public function createEmploye() {
        $employeType = $_POST['employeType'];
        $prenom = $_POST['prenom'];
        $nom = $_POST['nom'];
        $login = $_POST['login'];
        $password = $_POST['password'];

        $this->employeModel->create($employeType, $prenom, $nom, $login, $password);

        header('Location: /employe');
    }

    public function createEmployeForm() {
        require dirname(__DIR__) . '/views/employeCreation.php';
    }
}