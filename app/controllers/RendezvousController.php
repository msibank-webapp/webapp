<?php
// Définition de la classe RendezvousController pour gérer les rendez-vous.
Class RendezvousController {
    // Déclarations de propriétés pour stocker des instances des modèles nécessaires.
    protected $clientModel;
    protected $RendezvousModel;
    protected $motifModel;
    protected $justificatifModel;
    protected $employeModel;

    // Constructeur de la classe qui initialise les modèles avec une connexion PDO.
    public function __construct($pdo) {
        $this->clientModel = new Client($pdo);
        $this->RendezvousModel = new Rendezvous($pdo);
        $this->motifModel = new Motif($pdo);
        $this->justificatifModel = new Justificatif($pdo);
        $this->employeModel = new Employe($pdo);
    }

    // Méthode pour afficher une page de création de rendez-vous sans données préchargées.
    public function createForm($conseiller_id) {
        $motifs = $this->motifModel->findAll();
        $clients = $this->clientModel->getClientListByConseiller($conseiller_id);

        require dirname(__DIR__) . '/views/rendezvousCreation.php';
    }

    // Méthode pour créer un rendez-vous en utilisant les données POST du formulaire.
    public function create() {
        $conseiller_id = $_POST['conseiller_id'];
        $agent_id = $_POST['agent_id'];
        $motif_id = $_POST['motif_id'];
        $client_id = $_POST['client_id'];
        $dateDebut = $_POST['dateDebut'];
        $dateFin = $_POST['dateFin'];

        // Tente de créer un rendez-vous avec les données fournies (try). En cas d'erreur, renvois sur le formulaire de création avec un message d'erreur (catch).
        try {
            $idRendezvous = $this->RendezvousModel->create($conseiller_id, $agent_id, $motif_id, $client_id, $dateDebut, $dateFin);
        } catch (Exception $e) {
            $error = $e->getMessage();
            $motifs = $this->motifModel->findAll();
            $clients = $this->clientModel->getClientListByConseiller($conseiller_id);
            require dirname(__DIR__) . '/views/rendezvousCreation.php';
            return;
        }
        header('Location: /rendezvous/detail?id=' . $idRendezvous);
    }

    public function showRendezvousDetail($id) {
        $rendezvous = $this->RendezvousModel->findById($id);
        $motif = $this->motifModel->findById($rendezvous['Id_Motif']);
        $justificatifs = $this->motifModel->findJustificatifsByMotifId($rendezvous['Id_Motif']);
        $conseiller = $this->employeModel->findConseillerById($rendezvous['Id_Conseiller']);
        $agent = $this->employeModel->findAgentById($rendezvous['Id_Agent']);
        $client = $this->clientModel->findById($rendezvous['Id_Client']);

        $dateJour = date('Y-m-d', strtotime($rendezvous['Date_debut']));
        $dateHeureDebut = date('H:i', strtotime($rendezvous['Date_debut']));
        $dateHeureFin = date('H:i', strtotime($rendezvous['Date_fin']));

        require dirname(__DIR__) . '/views/rendezvousDetail.php';
    }
}