<?php 

class AccueilController {

 //Initialisation des propriétés pour la construction objet
    public function __construct() {
    }

//Appel de l'affichage de la page d'accueil de l'application
    public function index() {
        require dirname(__DIR__) . '/views/accueil.php';
    }
}
?>