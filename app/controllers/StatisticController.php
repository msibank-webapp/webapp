<?php 

require_once dirname(__DIR__) . '/models/Contrat.php';
require_once dirname(__DIR__) . '/models/Rendezvous.php';
require_once dirname(__DIR__) . '/models/Client.php';

// Définit la classe StatisticController pour gérer les statistiques.
Class StatisticController {
    // Propriété pour stocker l'instance de PDO pour les opérations de base de données.
    protected $contratModel;
    protected $rendezvousModel;
    protected $clientModel;

    // Constructeur de la classe qui initialise la propriété PDO avec l'instance PDO passée en paramètre.
    public function __construct($pdo) {
        // Assignation de l'instance PDO à la propriété de la classe.
        $this->contratModel = new Contrat($pdo);
        $this->rendezvousModel = new Rendezvous($pdo);
        $this->clientModel = new Client($pdo);
    }

    public function showStatisticForm() {
        require dirname(__DIR__) . '/views/statistiqueForm.php';
    }

    public function generate($data) {
        $startDate = $data['startDate'];
        $endDate = $data['endDate'];
        $dateClientCount = $data['onDate'];

        $statistiques = [
            'NbContrat' => $this->contratModel->getNbContratBetweenDate($startDate, $endDate),
            'NbRendezvous' => $this->rendezvousModel->getNbRendezvousBetweenDate($startDate, $endDate),
            'NbClient' => $this->clientModel->getNbClientForDate($dateClientCount)
        ];

        require dirname(__DIR__) . '/views/statistique.php';
    }
}