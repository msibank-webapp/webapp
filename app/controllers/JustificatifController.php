<?php

// Inclut le modèle Justificatif pour pouvoir accéder à ses méthodes.
require_once dirname(__DIR__) . '/models/Justificatif.php';

// Définit la classe JustificatifController pour gérer les actions liées aux justificatifs.
Class JustificatifController {
    // Propriété pour stocker une instance du modèle Justificatif.
    protected $justificatifModel;

    // Constructeur de la classe qui initialise le modèle Justificatif avec une connexion PDO.
    public function __construct($pdo) {
        // Crée une instance du modèle Justificatif avec l'objet PDO pour gérer la base de données.
        $this->justificatifModel = new Justificatif($pdo);
    }

      // Méthode pour récupérer et afficher la liste de tous les justificatifs.
    public function showJustificatifList() {
        // Récupère tous les justificatifs en utilisant la méthode findAll du modèle.
        $justificatifs = $this->justificatifModel->findAll();

         // Charge la vue qui présente la liste des justificatifs.
        require dirname(__DIR__) . '/views/JustificatifList.php';
    }

    // Méthode pour créer un nouveau justificatif à partir des données reçues via POST.
    public function createJustificatif() {
        // Récupère le nom du justificatif à partir des données envoyées en POST.
        $name = $_POST['name'];
         // Appelle la méthode de création du modèle pour ajouter un nouveau justificatif dans la base de données.
        $this->justificatifModel->create($name);

         // Redirige l'utilisateur vers la liste des justificatifs après l'ajout.
        header('Location: /justificatif');
    }

    // Méthode pour afficher le formulaire de création d'un nouveau justificatif.
    public function createJustificatifForm() {
        // Charge la vue qui contient le formulaire de création de justificatif.
        require dirname(__DIR__) . '/views/JustificatifCreation.php';
    }
}