<?php

// Importation des trois modèles 
// Garanti que les fichiers sont inclus une seule fois et donc pas de redéclarations
require_once dirname(__DIR__) . '/models/TacheAdministrative.php';
require_once dirname(__DIR__) . '/models/RendezVous.php';
require_once dirname(__DIR__) . '/models/Employe.php';

class PlanningController
{

    // Stockage des instances, permet d'intéragir avec la BDD et les rendez-vous. 
    // Accéssibilité des classes elles-mêmes et des classes dérivées 
    protected $tacheModel;
    protected $rendezVousModel;
    protected $employeModel;

    // Utilisation d'une même connexion à la BDD pour les opérations relatives aux tâches admin et RDV
    public function __construct($pdo)
    {
        $this->tacheModel = new TacheAdministrative($pdo);
        $this->rendezVousModel = new RendezVous($pdo);
        $this->employeModel = new Employe($pdo);
    }

    // Contrôle de la planification 
    public function showConseillerPlanning($conseiller_id)
    {
        // Calcule des dates de début et de fin de la semaine courante ou d'une semaine spécifique
        if (isset($_GET['week'])) {
            $weekModifier = (int)$_GET['week'];
            $weekStart = date('Y-m-d', strtotime("monday this week +{$weekModifier} week"));
            $weekEnd = date('Y-m-d', strtotime("sunday this week +{$weekModifier} week"));
        } else {
            $weekModifier = 0;
            $weekStart = date('Y-m-d', strtotime('monday this week'));
            $weekEnd = date('Y-m-d', strtotime('sunday this week'));
        }

        // Constructions des URLs pour la navigation à la semaine précédente et suivante.
        $prevWeekUrl = '/planning?conseiller_id=' . $conseiller_id . '&week=' . ($weekModifier - 1);
        $nextWeekUrl = '/planning?conseiller_id=' . $conseiller_id . '&week=' . ($weekModifier + 1);
        // Récupération des tâches et des rendez-vous pour la période donnée
        $conseiller = $this->employeModel->findConseillerById($conseiller_id);
        $taches = $this->tacheModel->getTasksForWeek($conseiller_id, $weekStart, $weekEnd);
        $rendezvous = $this->rendezVousModel->getAppointmentsForWeek($conseiller_id, $weekStart, $weekEnd);

        // Changement de vue, affichage du planning 
        require dirname(__DIR__) . '/views/planning.php';
    }

    public function showConseillerSelect()
    {
        $conseillers = $this->employeModel->getConseillerList();
        require dirname(__DIR__) . '/views/planningByConseilller.php';
    }
}
