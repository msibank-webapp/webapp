<?php

// Inclusion du fichier contenant le modèle qui gère les opérations de BDD et tâche admin
require_once dirname(__DIR__) . '/models/TacheAdministrative.php';

// Gestion des requêtes concernant les tâches admin (Opérations CRUD)
class TacheAdministrativeController
{

    protected $tacheModel;

    public function __construct($pdo)
    {
        $this->tacheModel = new TacheAdministrative($pdo);
    }

    // Mise à jour de la création de tâches admin 
    public function create()
    {
        $dateDebut = $_POST['dateDebut'];
        $dateFin = $_POST['dateFin'];
        $libelle = $_POST['libelle'];
        $conseillerId = $_SESSION['roleId'];

        try {
        $this->tacheModel->create($dateDebut, $dateFin, $libelle, $conseillerId);
        } catch (Exception $e) {
            $error = $e->getMessage();
            require dirname(__DIR__) . '/views/tacheadministrativeCreate.php';
            return;
        }

        header('Location: /planning?conseiller_id=' . $conseillerId);
    }

    // Charge une vue spécifique destinée à afficher le formulaire de création d'une nouvelle tâche admin
    public function createForm()
    {
        require dirname(__DIR__) . '/views/tacheadministrativeCreate.php';
    }

    // Récupération de la tâche correspondante à l'ID 
    // Vérification si l'utilisateur connecté est le conseiller qui a créé la tâche
    public function updateForm($id)
    {
        $task = $this->tacheModel->findById($id);

        // On s'assure que l'id appartient à un conseiller 
        if ($_SESSION['roleId'] != $task['Id_Conseiller']) {
            // Redirect or show an error message
            die('Accès non autorisé');
        }

        require dirname(__DIR__) . '/views/tacheadministrative.php';
    }

    // Mise à jour des tâches admin déjà existant 
    public function update()
    {
        $id = $_POST['id'];
        $dateDebut = $_POST['dateDebut'];
        $dateFin = $_POST['dateFin'];
        $libelle = $_POST['libelle'];
        $idConseiller = $_POST['conseiller_id'];

        try {
        $this->tacheModel->update($id, $dateDebut, $dateFin, $libelle, $idConseiller);
        } catch (Exception $e) {
            $error = $e->getMessage();
            $task = $this->tacheModel->findById($id);
            require dirname(__DIR__) . '/views/tacheadministrative.php';
            return;
        }

        header('Location: /planning?conseiller_id=' . $idConseiller);
    }

    // Mise à jours de la suppression de tâches admin 
    public function delete()
    {
        $id = $_POST['id'];

        $this->tacheModel->delete($id);

        header('Location: /planning?conseiller_id=' . $_SESSION['roleId']);
    }
}
