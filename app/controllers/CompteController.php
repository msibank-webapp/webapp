<?php
// Inclusion des fichiers modèle pour les classes Compte et Operation.
require_once dirname(__DIR__) . '/models/Compte.php';
require_once dirname(__DIR__) . '/models/Operation.php';
// Déclaration de la classe CompteController pour la gestion des comptes.
class CompteController {
    // Propriétés pour stocker les instances des modèles Compte et Operation.
    protected $compteModel;
    protected $operationModel;

    // Constructeur qui initialise les modèles avec une connexion PDO passée en paramètre.
    public function __construct($pdo) {
        $this->compteModel = new Compte($pdo);
        $this->operationModel = new Operation($pdo);
    }

    // Méthode pour afficher les détails d'un compte unique identifié par son ID.
    public function showUnique($id) {
        // Récupération des détails du compte par son ID.
        $compte = $this->compteModel->findById($id);
        // Récupération des opérations associées au compte.
        $operations = $this->operationModel->getOperationsByCompteId($id);

        // Inclusion de la vue qui affiche les détails du compte.
        require dirname(__DIR__) . '/views/compteDetail.php';
    }

    // Méthode pour préparer et afficher la vue pour la création d'un nouveau compte.
    public function showEmpty() {
        // Récupération des types de compte disponibles pour la création d'un nouveau compte.
        $compteTypes = $this->compteModel->getCompteTypes();
        // Inclusion de la vue pour la création de compte.
        require dirname(__DIR__) . '/views/compteCreation.php';
    }
    
    // Méthode pour créer un nouveau compte avec les données fournies via POST.
    public function create($clientId) {
        // Récupération des valeurs nécessaires à la création d'un compte depuis le formulaire.
        $compteType = $_POST['compteType'];
        $solde = $_POST['solde'];
        $clientId = $_POST['client_id'];
        $dateOuverture = date('Y-m-d H:i:s');
        $decouvert = $_POST['decouvert'];

         // Création du compte via le modèle.
        $this->compteModel->create($clientId, $compteType, $solde, $dateOuverture, $decouvert);

        // Redirection vers la page du client après la création du compte.
        header('Location: /client?id=' . $clientId);
    }

    // Méthode pour fermer un compte spécifique par son ID.
    public function close($id) {
        // Définition de la date de fermeture du compte.
        $dateFermeture = date('Y-m-d H:i:s');

         // Fermeture du compte via le modèle.
        $this->compteModel->closeAccount($id, $dateFermeture);
        // Redirection vers la page détaillée du compte.
        header('Location: /compte?id=' . $id);
    }

    // Méthode pour préparer et afficher la vue de création d'une nouvelle opération sans données préchargées.
    public function newEmptyOperation() {
        // Récupération de l'ID du compte via GET.
        $compteId = $_GET['compte_id'];
        // Inclusion de la vue pour la création d'une opération.
        require dirname(__DIR__) . '/views/operationCreation.php';
    }

    // Méthode pour créer une nouvelle opération sur un compte spécifique.
    public function newOperation() {
        // Récupération des détails de l'opération depuis le formulaire.
        $compteId = $_POST['compteId'];
        $montant = $_POST['montant'];
        $type = $_POST['type'];
        $date = date('Y-m-d H:i:s');

         // Vérification du solde avant l'opération, création de l'opération et mise à jour du solde.
        try {
            $this->compteModel->checkSolde($compteId, $montant, $type);
        } catch (Exception $e) {
            $error = $e->getMessage();
            require dirname(__DIR__) . '/views/operationCreation.php';
            return;
        }
        $this->operationModel->create($compteId, $montant, $type, $date);
        $this->compteModel->updateSolde($compteId, $montant, $type);
        
        header('Location: /compte?id=' . $compteId);
    }

    public function updateForm($idCompte) {
        $compte = $this->compteModel->findById($idCompte);
        require dirname(__DIR__) . '/views/compteUpdate.php';
    }

    public function update() {
        $compteId = $_POST['compteId'];
        $decouvert = $_POST['decouvert'];


        $this->compteModel->update($compteId, $decouvert);

        header('Location: /compte?id=' . $compteId);
    }
}