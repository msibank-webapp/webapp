<?php
// Importe la classe Contrat depuis son fichier source.
require_once dirname(__DIR__) . '/models/Contrat.php';

// Définition de la classe ContratController
class ContratController {
    // Propriété pour stocker une instance du modèle Contrat.
    protected $contratModel;
 // Constructeur de la classe qui initialise le modèle Contrat avec une connexion PDO passée en paramètre.
    public function __construct($pdo) {
        $this->contratModel = new Contrat($pdo);
    }
 // Méthode pour afficher la page de création de contrat sans données préchargées.
    public function showEmpty() {
        // Appel de la méthode pour obtenir les types de contrat du modèle.
        $contratTypes = $this->contratModel->getContratTypes();
       // Inclut le fichier de vue pour la création de contrat.
        require dirname(__DIR__) . '/views/contratCreation.php';
    }
  // Méthode pour afficher les détails d'un contrat unique en utilisant son identifiant.
    public function showUnique($id) {
        // Récupère le contrat spécifique par son ID via le modèle.
        $contrat = $this->contratModel->findById($id);
        // Charge la vue de détail du contrat.
        require dirname(__DIR__) . '/views/contratDetail.php';
    }
    
     // Méthode pour créer un contrat en utilisant les données POST du formulaire.
    public function create($clientId) {
         // Récupère le type de contrat à partir des données POST.
        $type = $_POST['contratType'];
         // Récupère l'identifiant du client à partir des données POST (réassigné malgré le paramètre).
        $clientId = $_POST['client_id'];
        // Récupère le libellé du contrat à partir des données POST.
        $libelle = $_POST['libelle'];
        // Fixe la date de début du contrat à la date du jour.
        $dateDebut = date('Y-m-d');
        // Récupère le tarif du contrat à partir des données POST.
        $tarif = $_POST['tarif'];

        // Appelle la méthode de création de contrat du modèle avec les données collectées.
        $this->contratModel->create($clientId, $type, $dateDebut, $tarif, $libelle);

        // Redirige l'utilisateur vers la page de détail du client.
        header('Location: /client?id=' . $clientId);
    }

    // Méthode pour fermer un contrat, identifié par son ID.
    public function close($id) {
       // Définit la date et heure actuelle de fermeture.
        $dateFermeture = date('Y-m-d H:i:s');

         // Appelle la méthode du modèle pour fermer le contrat avec la date de fermeture.
        $this->contratModel->closeContrat($id, $dateFermeture);
        // Redirige vers la page de détail du contrat fermé.
        header('Location: /contrat?id=' . $id);
    }
}