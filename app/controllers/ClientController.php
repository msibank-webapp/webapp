<?php

//Importation des 3 modèles 
require_once dirname(__DIR__) . '/models/Client.php'; //renvoie le chemin du dossier pardent du dosisser où se trouve le script acutel
require_once dirname(__DIR__) . '/models/Contrat.php';
require_once dirname(__DIR__) . '/models/Compte.php';

class ClientController { //Definition de la classe qui gère les requêtes relatives aux clients dans l'application 

    //déclarent 3 propriétés protégées dans la classe
    protected $clientModel;//Protection pour resteindre l'accès direct à ces propriétés depuis l'extérieur de la classe ou ses sous-classes
    protected $contratModel;
    protected $compteModel;
    protected $employeModel;

//$pdo est un objet, instance de connexion à la base de donnée (PDO - PHP Data Objects)
// chaque modèle interagi avec la base de données 
    public function __construct($pdo) {
        $this->clientModel = new Client($pdo);
        $this->contratModel = new Contrat($pdo);
        $this->compteModel = new Compte($pdo);
        $this->employeModel = new Employe($pdo);
    }

//récupération d'une chaîne de recherche depuis le tableau superglobal $_GET
    public function showClientList() {
        $search = $_GET['search'] ?? ''; //filtre les clients 
        $clients = $this->clientModel->getClientList($search);//récupère la liste des clients filtrée par la chaîne de recherche 

        require dirname(__DIR__) . '/views/client.php';//charge la vue client.php pour afficher les résultats
    }

    public function showUnique($id) {
        $client = $this->clientModel->findById($id);//récupération des détails d'un client spécifique par son id
        $comptes = $this->compteModel->getCompteListByClientID($id);//récupération des informations du client sur les comptes
        $contrats = $this->contratModel->getContratListByClientID($id);//récupération des informations du client sur les contrats

        require dirname(__DIR__) . '/views/clientDetail.php';//charge la vue afin d'afficher les informations/les résultats 
    }

    public function createClientform() {
        $conseillers = $this->employeModel->getConseillerList();//récupération de la liste des conseillers pour le formulaire de création de client
        require dirname(__DIR__) . '/views/clientCreation.php';//charge la vue clientCreate.php pour afficher le formulaire de création de client
    }

    public function updateClientform() {
        $id = $_GET['client_id'];//récupération de l'id du client à mettre à jour
        $client = $this->clientModel->findById($id);// récupération des détails du client à mettre à jour
        $conseillers = $this->employeModel->getConseillerList();// récupération de la liste des conseillers pour le formulaire de mise à jour de client
        $currentConseiller = $this->employeModel->findConseillerById($client['Id_Conseiller']); // récupération du conseiller actuel du client

        require dirname(__DIR__) . '/views/clientUpdate.php';// charge la vue clientUpdate.php pour afficher le formulaire de mise à jour de client
    }

    public function createClient() {
        $prenom = $_POST['prenom'];//récupération des données du formulaire de création de client
        $nom = $_POST['nom'];
        $date_de_naissance = $_POST['date_de_naissance'];
        $profession = $_POST['profession'];
        $num_tel = $_POST['num_tel'];
        $mail = $_POST['mail'];
        $adresse = $_POST['adresse'];
        $situation_familliale = $_POST['situation_familliale'];
        $idConseiller = $_POST['conseillerId'];
        $date_creation = date('Y-m-d'); //récupération de la date actuelle

        $newClientid = $this->clientModel->createClient($prenom, $nom, $date_de_naissance, $profession, $num_tel, $mail, $adresse, $situation_familliale, $date_creation, $idConseiller);//création d'un client avec les données récupérées

        header('Location: /client?id=' . $newClientid); //redirection vers la liste des clients
    }

    public function updateClient() {
        $id = $_POST['id'];//récupération des données du formulaire de mise à jour de client
        $prenom = $_POST['prenom'];
        $nom = $_POST['nom'];
        $date_de_naissance = $_POST['date_de_naissance'];
        $profession = $_POST['profession'];
        $num_tel = $_POST['num_tel'];
        $mail = $_POST['mail'];
        $adresse = $_POST['adresse'];
        $situation_familliale = $_POST['situation_familliale'];
        $idConseiller = $_POST['conseillerId'];

        $this->clientModel->updateClient($id, $prenom, $nom, $date_de_naissance, $profession, $num_tel, $mail, $adresse, $situation_familliale, $idConseiller);//mise à jour des informations du client

        header('Location: /client?id=' . $id);//redirection vers la liste des clients
    }
}
?>