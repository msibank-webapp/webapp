<?php

// Inclut les modèles Motif et Justificatif pour accéder à leurs méthodes.
require_once dirname(__DIR__) . '/models/Motif.php';
require_once dirname(__DIR__) . '/models/Justificatif.php';

// Déclaration de la classe MotifController pour la gestion des motifs.
class MotifController
{
    // Propriétés pour stocker les instances des modèles Motif et Justificatif.
    protected $motifModel;
    protected $justificatifModel;

    // Constructeur qui initialise les modèles avec une connexion PDO passée en paramètre.
    public function __construct($pdo) {
        // Création des instances de modèle avec l'objet PDO.
        $this->motifModel = new Motif($pdo);
        $this->justificatifModel = new Justificatif($pdo);
    }

    // Méthode pour afficher la liste complète des motifs.
    public function showMotifList() {
         // Récupère différents types de motifs à partir de la base de données.
        $typeComptes = $this->motifModel->findAllCompteType();
        $typeContrats = $this->motifModel->findAllContratType();
        $otherMotifs = $this->motifModel->findAllOtherMotifs();

         // Charge la vue qui présente tous les motifs.
        require dirname(__DIR__) . '/views/Motif.php';
    }
    // Méthode pour afficher les détails d'un motif spécifique par son ID.
    public function showMotif($id) {
        // Récupération d'un motif spécifique et ses justificatifs associés.
        $motif = $this->motifModel->findById($id);
        $justificatifs = $this->motifModel->findJustificatifsByMotifId($id);

        // Charge la vue de détail pour un motif.
        require dirname(__DIR__) . '/views/MotifDetail.php';
    }

    // Méthode pour créer un nouveau motif à partir des données reçues via POST.
    public function createMotif() {
        // Récupération des données nécessaires à la création d'un motif.
        $motifName = $_POST['motif'];
        $typeMotif = $_POST['typeMotif'];
        $Libelle = $typeMotif === "Autre" ? $motifName : null;
        $justificatifs = isset($_POST['justificatifs']) ? $_POST['justificatifs'] : [];

        // Création du motif et liaison avec les justificatifs correspondants.
        $idMotif = $this->motifModel->create($motifName, $typeMotif, $Libelle);
        foreach ($justificatifs as $justificatif) {
            $this->motifModel->linkJustificatifWithMotif($idMotif, $justificatif);
        }
        // Redirection vers la liste des motifs.
        header('Location: /motif');
    }

     // Méthode pour afficher le formulaire de création de motif.
    public function createMotifForm()
    {
        // Récupération de tous les justificatifs disponibles pour inclusion dans le formulaire.
        $justificatifs = $this->justificatifModel->findAll();
         // Charge la vue de création de motif.
        require dirname(__DIR__) . '/views/MotifCreation.php';
    }

    // Méthode pour afficher le formulaire pour lier des justificatifs à un motif existant.
    public function linkJustificatifForm($id)
    {
        // Récupération du motif par son ID et de tous les justificatifs disponibles.
        $motif = $this->motifModel->findById($id);
        $actualJustificatifs = $this->motifModel->findJustificatifsByMotifId($id);
        $justificatifs = $this->justificatifModel->findAll();
        // Charge la vue pour lier des justificatifs à un motif.
        require dirname(__DIR__) . '/views/JustificatifLinkToMotif.php';
    }

    // Méthode pour lier des justificatifs à un motif existant en utilisant les données POST.
    public function linkJustificatif() {
        // Récupération de l'ID du motif et des justificatifs sélectionnés.
        $idMotif = $_POST['motif_id'];
        $justificatifs = $_POST['justificatifs'];
        $actualJustificatifs = $this->motifModel->findJustificatifsByMotifId($idMotif);

       // Supprime les justificatifs qui étaient selectionnés et qui ne le sont plus pour ce motif.
       // Par exemple, si un justificatif était lié à un motif et n'est plus sélectionné, il sera supprimé.
        foreach ($actualJustificatifs as $actualJustificatif) {
            if (!in_array($actualJustificatif['Id_Piece_a_fournir'], $justificatifs)) {
                $this->motifModel->unlinkJustificatifFromMotif($idMotif, $actualJustificatif['Id_Piece_a_fournir']);
            }
        }

        // Ajoute les nouveaux justificatifs sélectionnés qui ne sont pas déjà liés au motif.
        foreach ($justificatifs as $justificatif) {
            if (!in_array($justificatif, array_column($actualJustificatifs, 'Id_Piece_a_fournir'))) {
                $this->motifModel->linkJustificatifWithMotif($idMotif, $justificatif);
            }
        }


        // Redirige le navigateur vers une URL spécifique, ici à la page de détail du motif avec son ID.
        // Cela permet à l'utilisateur de voir immédiatement le motif mis à jour avec les justificatifs liés.
        header('Location: /motif?id=' . $idMotif);
    }
}
