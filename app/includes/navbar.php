<nav class="navbar">
    <div class="navbar-container"> <!-- Permet de grouper les autres elements pour une meilleure lisibilité -->
        <a href="accueil" class="navbar-logo">
            <img src="/public/images/logo.png" alt="Logo" />
        </a>
        <div class="navbar-links">
            <a href="/accueil">Accueil</a>
            <a href="/client">Clients</a>
            <a href="/planning">Planning</a>
            <?php if ($_SESSION['role'] == 'Directeur') : ?>
            <a href="/employe">Employés</a>
            <a href="/statistique">Statistiques</a>
            <a href="/motif">Motifs de rendez-vous</a>
            <a href="/justificatif">Justificatifs de rendez-vous</a>
            <?php endif; ?>
        </div>
        <div class="navbar-user" onclick="toggleDropdown()"> <!-- Menu utilisateur déroulant -->
            <img src="/public/images/circle-user-solid.svg" alt="User" />
            <div id="userDropdown" class="dropdown-content"> <!-- contenu du menu déroulant -->
                <a href="/public/logout.php" class="logout"><span class="icon"><i class="fa-solid fa-right-from-bracket"></i></span><strong>Deconnexion</strong></a>
            </div>
        </div>
    </div>
</nav>

<script>
    function toggleDropdown() { //fonction pour ouvrir/fermer le menu déroulant
        document.getElementById("userDropdown").classList.toggle("show"); //la classe "show" contrôle la visibilité du menu déroulant
    }
    //Determine s'il y a un clique sur la page, ce qui ferme le menu
    window.onclick = function(event) { 
        if (!event.target.matches('.navbar-user img')) { // Vérifie si l'élément cliqué n'est pas l'image dans la barre de navigation de l'utilisateur
            var dropdowns = document.getElementsByClassName("dropdown-content");  // Récupère tous les éléments ayant la classe 'dropdown-content'
            for (var i = 0; i < dropdowns.length; i++) {
                var openDropdown = dropdowns[i];
                if (openDropdown.classList.contains('show')) { // Vérifie si le menu déroulant actuel est ouvert (contient la classe 'show')
                    openDropdown.classList.remove('show'); //enlève la classe "show" pour fermer le menu 
                }
            }
        }
    }
</script>