<?php
$role = $_SESSION['role'];
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title>Accueil</title>
    <link rel="stylesheet" href="/public/css/base.css">
    <script src="https://kit.fontawesome.com/ac37d65e1e.js" crossorigin="anonymous"></script> <!-- pour récuperer les icones -->
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600&display=swap" rel="stylesheet"> <!-- police d'écriture -->
</head>

<body>
    <h1>Désolé</h1>
    <p>En tant que <?= $role ?>, vous n'êtes pas autorisé à acceder à cette page</p>
</body>
</html>