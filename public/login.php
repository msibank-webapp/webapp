<?php
session_start(); //récupère les erreurs de sessions (ex: si on se trompe de mdp)
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Login Page</title>
    <link rel="stylesheet" href="/public/css/login.css">
</head>

<body>
    <div id="login-form">
        <h2>Connexion</h2>
        <form action="authenticate.php" method="POST"> <!-- méthode : envoie -->
            <div class="form-group">
                <label for="username">Nom d'utilisateur:</label>
                <input type="text" id="username" name="username" required>
            </div>
            <div class="form-group">
                <label for="password">Mot de passe:</label>
                <input type="password" id="password" name="password" required>
            </div>
            <?php if (isset($_SESSION['error'])) : ?>
                <div class="error-message">
                    <?php
                    echo $_SESSION['error']; // Display error message 
                    unset($_SESSION['error']); // Clear error message from session
                    ?>
                </div>
            <?php endif; ?>
            <button type="submit">Connexion</button>
        </form>
    </div>
</body>

</html>