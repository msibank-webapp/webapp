<?php //pour faire le lien avec la BDD
session_start();
require '../config/db.php'; // on charge le fichier db.php avant

$username = $_POST['username']; 
$password = $_POST['password'];


$stmt = $pdo->prepare("SELECT Id_Employe, Login, Role, Password, Prenom, Nom FROM Employe WHERE Login = ?"); //pdo session avec notre BDD : prepare = methode qui prend en argument une requete sql
$stmt->execute([$username]); //pour remplacer le ?
$user = $stmt->fetch(); //sotcker le résultat de la requête

if ($user && $password === $user['Password']) { //si le password entré est = au password de l'utilisateur
    $_SESSION['loggedin'] = true;
    $_SESSION['prenom'] = $user['Prenom'];
    $_SESSION['nom'] = $user['Nom'];
    $_SESSION['id_employe'] = $user['Id_Employe'];
    $_SESSION['username'] = $user['Login'];
    $_SESSION['role'] = $user['Role'];

    switch ($user['Role']) {
        case 'Conseiller':
            $_SESSION['roleId'] = $pdo->query("SELECT Id_Conseiller FROM Conseiller WHERE Id_Employe = '{$user['Id_Employe']}'")->fetchColumn();
            break;
        case 'Agent':
            $_SESSION['roleId'] = $pdo->query("SELECT Id_Agent FROM Agent WHERE Id_Employe = '{$user['Id_Employe']}'")->fetchColumn();
            break;
        case 'Directeur':
            $_SESSION['roleId'] = $pdo->query("SELECT Id_Directeur FROM Directeur WHERE Id_Employe = '{$user['Id_Employe']}'")->fetchColumn();
            break;
    }

    header('Location: /accueil');// redirige l'utilisateur vers la page d'accueil.
    exit;
}
$_SESSION['loggedin'] = false; //définir des variables de session pour le message d'erreur de connexion et redirigent l'utilisateur vers la page de connexion
$_SESSION['error'] = 'Mot de passe ou utilisateur incorrect.';
header('Location: /public/login.php');
