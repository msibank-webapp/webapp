<?php
// démarre une nouvelle session ou reprend une session existante.
session_start();
//supprime toutes les variables de session existantes.
session_unset();
// détruit la session actuelle et supprime toutes les variables de session associées
session_destroy();
//définit l'en-tête de localisation HTTP, ce qui redirige l'utilisateur vers la page de connexion
header('Location: /public/login.php');
//arrête l'exécution du script PHP actuel
exit;
